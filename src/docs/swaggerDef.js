const { version } = require('../../package.json');
const config = require('../config/config');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'OPTIMI-HEALTH API documentation',
    version: '1.0.0',
    license: {
      name: 'MIT',
      url: 'https://github.com/hagopj13/node-express-boilerplate/blob/master/LICENSE',
    },
  },
  servers: [
    {
      url: `https://optimi-health-api.herokuapp.com/v1`,
    },
  ],
};

module.exports = swaggerDef;
