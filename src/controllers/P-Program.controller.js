const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { programService } = require('../services');
const response = require('../utils/response');

const createProgram = catchAsync(async (req, res, next) => {
  if (!req.file) {
 return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const program = await programService.createProgram(req.body);
  response.success(res, httpStatus.CREATED, program);
});

const deleteProgram = catchAsync(async (req, res) => {
  const program = await programService.deleteProgramById(req.params.programId);
  response.success(res, 200, program);
});

const getPrograms = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await programService.queryProgram(filter, options);
  response.success(res, 200, result);
});

const getProgram = catchAsync(async (req, res) => {
  const program = await programService.getProgramById(req.params.programId);
   response.success(res, 200, program);
});

const updateProgram = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const program = await programService.updateProgramById(req.params.programId, req.body);
  response.success(res, httpStatus.CREATED, program);
});



module.exports = {
   createProgram,
   deleteProgram,
   getPrograms,
   updateProgram,
   getProgram
}