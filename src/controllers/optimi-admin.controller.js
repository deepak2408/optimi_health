const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { adminService, tokenService } = require('../services');
const response = require('../utils/response');

const login = catchAsync(async (req, res) => {
  const log = await adminService.loginAdmin(req.body);
  const token = await tokenService.generateAuthTokens(log);
  response.success(res, 200, { log, token });
});

module.exports = {
  login,
};
