const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { sessionService } = require('../services');
const response = require('../utils/response');

const createSession = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const session = await sessionService.createSession(req.body);
  response.success(res, httpStatus.CREATED, session);
});

const deleteSession = catchAsync(async (req, res) => {
  const session = await sessionService.deleteSessionById(req.params.sessionId);
  response.success(res, 200, session);
});

const getSessions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await sessionService.querySession(filter, options);
  response.success(res, 200, result);
});

const getSession = catchAsync(async (req, res) => {
  const session = await sessionService.getSessionById(req.params.sessionId);
  response.success(res, 200, session);
});

const updateSession = catchAsync(async (req, res, next) => {
  if (!req.file) {
  return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const session = await sessionService.updateSessionById(req.params.sessionId, req.body);
  response.success(res, httpStatus.CREATED, session);
});



module.exports = {
   createSession,
   deleteSession,
   getSessions,
   updateSession,
   getSession
}