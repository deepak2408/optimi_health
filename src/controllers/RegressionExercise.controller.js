const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { regressionExerciseService } = require('../services');
const response = require('../utils/response');

const createRegressionExercise = catchAsync(async (req, res) => {
  const createRegExercise = await regressionExerciseService.createRegExercise(req.body);
  response.success(res, httpStatus.CREATED, createRegExercise);
});

const getRegressionExercise = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getExercise = await regressionExerciseService.getRegExercise(filter);
  response.success(res, 200, getExercise);
});

const deleteRegressionExercise = catchAsync(async (req, res) => {
  const deleteExercise = await regressionExerciseService.deleteRegExerciseById(req.params.regressionExerciseId);
  response.success(res, 200, deleteExercise);
});

const updateRegressionExercise = catchAsync(async (req, res) => {
    const updateRegExercise = await regressionExerciseService.updateRegExerciseById(
      req.params.regressionExerciseId,
      req.body
    );
    response.success(res, httpStatus.CREATED, updateRegExercise);
})

module.exports = {
    createRegressionExercise,
    getRegressionExercise,
    deleteRegressionExercise,
    updateRegressionExercise,
}