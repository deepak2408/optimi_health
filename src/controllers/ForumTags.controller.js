const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { forumTagsService } = require('../services');
const response = require('../utils/response');

const createForumTags = catchAsync(async (req, res) => {
  const forumTags = await forumTagsService.createForumTags(req.body);
  response.success(res, httpStatus.CREATED, forumTags);
});

const deleteForumTags = catchAsync(async (req, res) => {
  const forumTags = await forumTagsService.deleteForumTagsById(req.params.forumTagId);
  response.success(res, 200, forumTags);
});

const updateForumTags = catchAsync(async (req, res) => {
  const forumTags = await forumTagsService.updateForumTagsById(req.params.forumTagId, req.body);
  response.success(res, httpStatus.CREATED, forumTags);
});

const getForumTags = catchAsync(async (req, res) => {
  const forumTags = await forumTagsService.getAllForumTags(req.query);
  response.success(res, 200, forumTags);
});



module.exports = {
    createForumTags,
    deleteForumTags,
    updateForumTags,
    getForumTags,
}