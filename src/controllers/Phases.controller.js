const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { phasesService } = require('../services');
const response = require('../utils/response');

const createPhases = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const phases = await phasesService.createPhases(req.body);
  response.success(res, httpStatus.CREATED, phases);
});

const deletePhases = catchAsync(async (req, res) => {
  const phases = await phasesService.deletePhasesById(req.params.phaseId);
  response.success(res, 200, phases);
});

const getPhases = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title', 'description']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await phasesService.queryPhases(filter, options);
  response.success(res, 200, result);
});

const getPhase = catchAsync(async (req, res) => {
  const phases = await phasesService.getPhaseById(req.params.phaseId);
   response.success(res, 200, phases);
});

const updatePhases = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const phases = await phasesService.updatePhasesById(req.params.phaseId, req.body);
  response.success(res, httpStatus.CREATED, phases);
});



module.exports = {
   createPhases,
   deletePhases,
   getPhases,
   updatePhases,
   getPhase
}