const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { phaseTestService } = require('../services');
const response = require('../utils/response');

const createPhaseTest = catchAsync(async (req, res) => {
  const phaseTest = await phaseTestService.createPhaseTest(req.body);
  response.success(res, httpStatus.CREATED, phaseTest);
});

const deletePhaseTest = catchAsync(async (req, res) => {
  const phaseTest = await phaseTestService.deletePhaseTestById(req.params.phaseTestId);
  response.success(res, 200, phaseTest);
});

const updatePhaseTest = catchAsync(async (req, res) => {
  const phaseTest = await phaseTestService.updatePhaseTestById(req.params.phaseTestId, req.body);
  response.success(res, httpStatus.CREATED, phaseTest);
});

const getPhaseTest = catchAsync(async (req, res) => {
  const phaseTest = await phaseTestService.getAllPhaseTest(req.query);
  response.success(res, 200, phaseTest);
});



module.exports = {
    createPhaseTest,
    deletePhaseTest,
    updatePhaseTest,
    getPhaseTest,
}