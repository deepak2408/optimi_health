const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { bodyPartsService } = require('../services');
const response = require('../utils/response');

const createBodyParts = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const createBody = await bodyPartsService.createPartsOfBody(req.body);
  response.success(res, httpStatus.CREATED, createBody);
});

const getBodyParts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getBody = await bodyPartsService.getPartsOfBody(filter);
  response.success(res, 200, getBody);
});

const deleteBodyParts = catchAsync(async (req, res) => {
  const deleteBody = await bodyPartsService.deletePartsOfBodyById(req.params.bodyPartsId);
  response.success(res, 200, deleteBody);
});

const updateBodyParts = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const updateBody = await bodyPartsService.updatePartsOfBodyById(req.params.bodyPartsId, req.body);
  response.success(res, httpStatus.CREATED, updateBody);
});

module.exports = {
    createBodyParts,
    getBodyParts,
    deleteBodyParts,
    updateBodyParts,
}