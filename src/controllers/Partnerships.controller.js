const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { partnershipsService } = require('../services');
const response = require('../utils/response');

const createPartnerships = catchAsync(async (req, res) => {
  console.log(req.body);
  const partnerships = await partnershipsService.createPartnerships(req.body);
  response.success(res, httpStatus.CREATED, partnerships);
});

const deletePartnerships = catchAsync(async (req, res) => {
  const partnerships = await partnershipsService.deletePartnershipsById(req.params.partnershipId);
  response.success(res, 200, partnerships);
});

const updatePartnerships = catchAsync(async (req, res) => {
  const partnerships = await partnershipsService.updatePartnershipsById(req.params.partnershipId, req.body);
  response.success(res, httpStatus.CREATED, partnerships);
});

const getPartnerships = catchAsync(async (req, res) => {
  const partnerships = await partnershipsService.getAllPartnerships(req.query);
  response.success(res, 200, partnerships);
});



module.exports = {
    createPartnerships,
    deletePartnerships,
    updatePartnerships,
    getPartnerships,
}