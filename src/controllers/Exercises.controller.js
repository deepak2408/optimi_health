const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { exerciseService } = require('../services');
const response = require('../utils/response');

const createExercise = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const exercise = await exerciseService.createExercise(req.body);
  response.success(res, httpStatus.CREATED, exercise);
});

const deleteExercise = catchAsync(async (req, res) => {
  const exercise = await exerciseService.deleteExerciseById(req.params.exerciseId);
  response.success(res, 200, exercise);
});

const getExercises = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await exerciseService.queryExercises(filter, options);
  response.success(res, 200, result);
});

const getExercise = catchAsync(async (req, res) => {
  const exercise = await exerciseService.getExerciseById(req.params.exerciseId);
  response.success(res, 200, exercise);
});

const updateExercise = catchAsync(async (req, res, next) => {
  if (!req.file) {
     return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const exercise = await exerciseService.updateExerciseById(req.params.exerciseId, req.body);
  response.success(res, httpStatus.CREATED, exercise);
});



module.exports = {
   createExercise,
   deleteExercise,
   getExercises,
   updateExercise,
   getExercise
}