const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { programGoalService } = require('../services');
const response = require('../utils/response');

const createProgramGoal = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const createProgram = await programGoalService.createProgram(req.body);
  response.success(res, httpStatus.CREATED, createProgram);
});

const getProgramGoal = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getProgram = await programGoalService.getProgram(filter);
  response.success(res, 200, getProgram);
});

const deleteProgramGoal = catchAsync(async (req, res) => {
  const deleteProgram = await programGoalService.deleteProgramById(req.params.programGoalId);
  response.success(res, 200, deleteProgram);
});

const updateProgramGoal = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const updateProgram = await programGoalService.updateProgramById(req.params.programGoalId, req.body);
  response.success(res, httpStatus.CREATED, updateProgram);
});

module.exports = {
    createProgramGoal,
    getProgramGoal,
    deleteProgramGoal,
    updateProgramGoal,
}