const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { alternativeExerciseService } = require('../services');
const response = require('../utils/response');

const createAlternativeExercise = catchAsync(async (req, res) => {
  const createExercise = await alternativeExerciseService.createAltExercise(req.body);
  response.success(res, httpStatus.CREATED, createExercise);
});

const getAlternativeExercise = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getExercise = await alternativeExerciseService.getAltExercise(filter);
  response.success(res, 200, getExercise);
});

const deleteAlternativeExercise = catchAsync(async (req, res) => {
  const deleteExercise = await alternativeExerciseService.deleteAltExerciseById(req.params.alternativeExerciseId);
  response.success(res, 200, deleteExercise);
});

const updateAlternativeExercise = catchAsync(async (req, res) => {
  const updateExercise = await alternativeExerciseService.updateAltExerciseById(req.params.alternativeExerciseId, req.body);
  response.success(res, httpStatus.CREATED, updateExercise);
});

module.exports = {
    createAlternativeExercise,
    getAlternativeExercise,
    deleteAlternativeExercise,
    updateAlternativeExercise,
}