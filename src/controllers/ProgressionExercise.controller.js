const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { progressionExerciseService } = require('../services');
const response = require('../utils/response');

const createProgressionExercise = catchAsync(async (req, res) => {
  const createExercise = await progressionExerciseService.createProgExercise(req.body);
  response.success(res, httpStatus.CREATED, createExercise);
});

const getProgressionExercise = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getExercise = await progressionExerciseService.getProgExercise(filter);
  response.success(res, 200, getExercise);
});

const deleteProgressionExercise = catchAsync(async (req, res) => {
  const deleteExercise = await progressionExerciseService.deleteProgExerciseById(req.params.progressionExerciseId);
  response.success(res, 200, deleteExercise);
});

const updateProgressionExercise = catchAsync(async (req, res) => {
    const updateExercise = await progressionExerciseService.updateProgExerciseById(req.params.progressionExerciseId, req.body);
    response.success(res, httpStatus.CREATED, updateExercise);
})

module.exports = {
    createProgressionExercise,
    getProgressionExercise,
    deleteProgressionExercise,
    updateProgressionExercise,
}