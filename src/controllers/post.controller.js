const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { postService } = require('../services');
const response = require('../utils/response');

const uploadPic = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const posts = await postService.createPost(req.body);
  response.success(res, httpStatus.CREATED, posts);
});

const deletePosts = catchAsync(async (req, res) => {
  const posts = await postService.deletePostById(req.params.postId);
  response.success(res, 200, posts);
});

const getPosts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title', 'description']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await postService.queryPosts(filter, options);
  response.success(res, 200, result);
});

const getPost = catchAsync(async (req, res) => {
  const post = await postService.getPostById(req.params.postId);
  response.success(res, 200, post);
});

const updatePosts = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const posts = await postService.updatePostById(req.params.postId, req.body);
  response.success(res, httpStatus.CREATED, posts);
});



module.exports = {
   uploadPic,
   deletePosts,
   getPosts,
   updatePosts,
   getPost
}