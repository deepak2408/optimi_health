const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { exclusionCriteriaService } = require('../services');
const response = require('../utils/response');

const createExclusionCriteria = catchAsync(async (req, res) => {
  const createExCriteria = await exclusionCriteriaService.createExCriteria(req.body);
  response.success(res, httpStatus.CREATED, createExCriteria);
});

const getExclusionCriteria = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getExCriteria = await exclusionCriteriaService.getExCriteria(filter);
  response.success(res, 200, getExCriteria);
});

const deleteExclusionCriteria = catchAsync(async (req, res) => {
  const deleteExCriteria = await exclusionCriteriaService.deleteExCriteriaById(req.params.exclusionCriteriaId);
  response.success(res, 200, deleteExCriteria);
});

const updateExclusionCriteria = catchAsync(async (req, res) => {
    const updateExCriteria = await exclusionCriteriaService.updateExCriteriaById(req.params.exclusionCriteriaId, req.body);
     response.success(res, httpStatus.CREATED, updateExCriteria);
})

module.exports = {
    createExclusionCriteria,
    getExclusionCriteria,
    deleteExclusionCriteria,
    updateExclusionCriteria,
}