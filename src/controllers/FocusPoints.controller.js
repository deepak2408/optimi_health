const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { focusPointsService } = require('../services');
const response = require('../utils/response');

const createFocusPoints = catchAsync(async (req, res) => {
  const createFocus = await focusPointsService.createFocus(req.body);
  response.success(res, httpStatus.CREATED, createFocus);
});

const getFocusPoints = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getFocus = await focusPointsService.getFocus(filter);
  response.success(res, 200, getFocus);
});

const deleteFocusPoints = catchAsync(async (req, res) => {
  const deleteFocus = await focusPointsService.deleteFocusPointsById(req.params.focusPointsId);
  response.success(res, 200, deleteFocus);
});

const updateFocusPoints = catchAsync(async (req, res) => {
  const updateFocus = await focusPointsService.updateFocusPointsById(req.params.focusPointsId, req.body);
  response.success(res, httpStatus.CREATED, updateFocus);
});

module.exports = {
    createFocusPoints,
    getFocusPoints,
    deleteFocusPoints,
    updateFocusPoints,
}