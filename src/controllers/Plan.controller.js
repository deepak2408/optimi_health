const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { planService } = require('../services');
const response = require('../utils/response');

const createPlan = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const plan = await planService.createPlan(req.body);
  response.success(res, httpStatus.CREATED, plan);
});

const deletePlan = catchAsync(async (req, res) => {
  const plan = await planService.deletePlanById(req.params.planId);
  response.success(res, 200, plan);
});

const getPlans = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await planService.queryPlan(filter, options);
  response.success(res, 200, result);
});

const getPlan = catchAsync(async (req, res) => {
  const plan = await planService.getPlanById(req.params.planId);
  response.success(res, 200, plan);
});

const updatePlan = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const plan = await planService.updatePlanById(req.params.planId, req.body);
  response.success(res, httpStatus.CREATED, plan);
});

module.exports = {
  createPlan,
  deletePlan,
  getPlans,
  updatePlan,
  getPlan,
};
