const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { exerciseTypeService } = require('../services');
const response = require('../utils/response');

const createExerciseType = catchAsync(async (req, res) => {
  const createExercise = await exerciseTypeService.createExerciseTyp(req.body);
  response.success(res, httpStatus.CREATED, createExercise);
});

const getExerciseType = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getExercise = await exerciseTypeService.getExerciseTyp(filter);
  response.success(res, 200, getExercise);
});

const deleteExerciseType = catchAsync(async (req, res) => {
  const deleteExercise = await exerciseTypeService.deleteExerciseTypById(req.params.exerciseTypeId);
  response.success(res, 200, deleteExercise);
});

const updateExerciseType = catchAsync(async (req, res) => {
  const updateExercise = await exerciseTypeService.updateExerciseTypById(req.params.exerciseTypeId, req.body);
  response.success(res, httpStatus.CREATED, updateExercise);
});

module.exports = {
    createExerciseType,
    getExerciseType,
    deleteExerciseType,
    updateExerciseType,
}