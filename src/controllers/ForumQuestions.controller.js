const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { forumQuestionService } = require('../services');
const response = require('../utils/response');

const createForumQuestion = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const forumQuestion = await forumQuestionService.createForumQuestion(req.body);
  response.success(res, httpStatus.CREATED, forumQuestion);
});

const deleteForumQuestion = catchAsync(async (req, res) => {
  const forumQuestion = await forumQuestionService.deleteForumQuestionById(req.params.forumQuestionId);
  response.success(res, 200, forumQuestion);
});

const getForumQuestions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await forumQuestionService.queryForumQuestion(filter, options);
  response.success(res, 200, result);
});

const getForumQuestion = catchAsync(async (req, res) => {
  const forumQuestion = await forumQuestionService.getForumQuesitonById(req.params.forumQuestionId);
  response.success(res, 200, forumQuestion);
});

const updateForumQuestion = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const forumQuestion = await forumQuestionService.updateForumQuestionById(req.params.forumQuestionId, req.body);
  response.success(res, httpStatus.CREATED, forumQuestion);
});

module.exports = {
  createForumQuestion,
  deleteForumQuestion,
  getForumQuestions,
  updateForumQuestion,
  getForumQuestion,
};
