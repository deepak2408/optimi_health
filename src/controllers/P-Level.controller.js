const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { programLevelService } = require('../services');
const response = require('../utils/response');

const createProgramLevel = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const createProgram = await programLevelService.createProgram(req.body);
  response.success(res, httpStatus.CREATED, createProgram);
});

const getProgramLevel = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getProgram = await programLevelService.getProgram(filter);
  response.success(res, 200, getProgram);
});

const deleteProgramLevel = catchAsync(async (req, res) => {
  const deleteProgram = await programLevelService.deleteProgramById(req.params.programLevelId);
  response.success(res, 200, deleteProgram);
});

const updateProgramLevel = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const updateProgram = await programLevelService.updateProgramById(req.params.programLevelId, req.body);
  response.success(res, httpStatus.CREATED, updateProgram);
});

module.exports = {
    createProgramLevel,
    getProgramLevel,
    deleteProgramLevel,
    updateProgramLevel,
}