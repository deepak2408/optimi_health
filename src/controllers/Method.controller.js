const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { methodService } = require('../services');
const response = require('../utils/response');

const createMethod = catchAsync(async (req, res) => {
  const method = await methodService.createMethod(req.body);
  response.success(res, httpStatus.CREATED, method);
});

const deleteMethod = catchAsync(async (req, res) => {
  const Method = await methodService.deleteMethodById(req.params.methodId);
  response.success(res, 200, Method);
});

const updateMethod = catchAsync(async (req, res) => {
  const method = await methodService.updateMethodById(req.params.methodId, req.body);
  response.success(res, httpStatus.CREATED, method);
});

const getMethod = catchAsync(async (req, res) => {
  const method = await methodService.getAllMethod(req.query);
  response.success(res, 200, method);
});



module.exports = {
    createMethod,
    deleteMethod,
    updateMethod,
    getMethod,
}