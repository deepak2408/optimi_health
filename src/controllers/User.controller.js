const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { userService, tokenService } = require('../services');
const response = require('../utils/response');

const createUser = catchAsync(async (req, res) => {
  const createUser = await userService.createUser(req.body);
  const token = await tokenService.generateAuthTokens(createUser, createUser.isProfessional);
  response.success(res, httpStatus.CREATED, { createUser, token });
});

const getUser = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['firstName']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const getProgram = await userService.getUser(filter, options);
  response.success(res, 200, getProgram);
});

const deleteUser = catchAsync(async (req, res) => {
  const deleteUser = await userService.deleteUserById(req.params.userId);
  response.success(res, 200, deleteUser);
});

const login = catchAsync(async (req, res) => {
  const login = await userService.loginUser(req.body);
  const token = await tokenService.generateAuthTokens(login, login.isProfessional);
  response.success(res, 200, { login, token });
});

module.exports = {
  createUser,
  getUser,
  deleteUser,
  login,
};
