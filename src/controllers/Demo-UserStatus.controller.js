const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { demoUserStatusService } = require('../services');
const response = require('../utils/response');

const createDemoUser = catchAsync(async (req, res) => {
  const createDemoUserStatus = await demoUserStatusService.createDemo(req.body);
  response.success(res, 200, createDemoUserStatus);
});

module.exports = { createDemoUser };
