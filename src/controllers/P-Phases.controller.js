const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { programPhasesService } = require('../services');
const response = require('../utils/response');

const createProgramPhases = catchAsync(async (req, res) => {
  const createProgram = await programPhasesService.createProgram(req.body);
  response.success(httpStatus.CREATED, createProgram);
});

const getProgramPhases = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const getProgram = await programPhasesService.getProgram(filter);
  response.success(res, 200, getProgram);
});

const deleteProgramPhases = catchAsync(async (req, res) => {
    const deleteProgram = await programPhasesService.deleteProgramById(req.params.programPhaseId);
    res.send(deleteProgram);
})

const updateProgramPhases = catchAsync(async (req, res) => {
    const updateProgram = await programPhasesService.updateProgramById(req.params.programPhaseId, req.body);
    res.status(httpStatus.CREATED).send(updateProgram);
})

module.exports = {
    createProgramPhases,
    getProgramPhases,
    deleteProgramPhases,
    updateProgramPhases,
}