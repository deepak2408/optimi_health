const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { selfManageProgramService } = require('../services');
const response = require('../utils/response');

const createSelfManageProgram = catchAsync(async (req, res) => {
  const selfManage = await selfManageProgramService.createSelfManageProgram(req.body);
  response.success(res, httpStatus.CREATED, selfManage);
});

const deleteSelfManageProgram = catchAsync(async (req, res) => {
  const selfManage = await selfManageProgramService.deleteSelfManageProgramById(req.params.selfManageProgramId);
  response.success(res, 200, selfManage);
});

const getSelfManagePrograms = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await selfManageProgramService.querySelfManageProgram(filter, options);
  response.success(res, 200, result);
});

const getSelfManageProgram = catchAsync(async (req, res) => {
  const selfManage = await selfManageProgramService.getSelfManageProgramById(req.params.selfManageProgramId);
   response.success(res, 200, selfManage);
});

const updateSelfManageProgram = catchAsync(async (req, res) => {
  const selfManage = await selfManageProgramService.updateSelfManageProgramById(req.params.selfManageProgramId, req.body);
  response.success(res, httpStatus.CREATED, selfManage);
});

module.exports = {
  createSelfManageProgram,
  deleteSelfManageProgram,
  getSelfManagePrograms,
  updateSelfManageProgram,
  getSelfManageProgram,
};
