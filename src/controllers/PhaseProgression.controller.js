const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { phaseProgressionService } = require('../services');
const response = require('../utils/response');

const createPhaseProgression = catchAsync(async (req, res) => {
  const phaseProgression = await phaseProgressionService.createPhaseProgression(req.body);
  response.success(res, httpStatus.CREATED, phaseProgression);
});

const deletePhaseProgression = catchAsync(async (req, res) => {
  const phaseProgression = await phaseProgressionService.deletePhaseProgressionById(req.params.phaseProgressionId);
  response.success(res, 200, phaseProgression);
});

const updatePhaseProgression = catchAsync(async (req, res) => {
  const phaseProgression = await phaseProgressionService.updatePhaseProgressionById(req.params.phaseProgressionId, req.body);
  response.success(res, httpStatus.CREATED, phaseProgression);
});

const getPhaseProgression = catchAsync(async (req, res) => {
  const phaseProgression = await phaseProgressionService.getAllPhaseProgression(req.query);
  response.success(res, 200, phaseProgression);
});



module.exports = {
    createPhaseProgression,
    deletePhaseProgression,
    updatePhaseProgression,
    getPhaseProgression,
}