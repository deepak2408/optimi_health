const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { professionalService, tokenService } = require('../services');
const response = require('../utils/response');

const createProfessional = catchAsync(async (req, res) => {
  const professional = await professionalService.createProfessional(req.body);
  const tokens = await tokenService.generateAuthTokens(professional, professional.isProfessional);
  response.success(res, httpStatus.CREATED, { professional, tokens });
});

const getProfessionals = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['email, phoneNo']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const getProgram = await professionalService.getProfessional(filter, options);
  response.success(res, 200, getProgram);
});

const deleteProfessional = catchAsync(async (req, res) => {
  const professional = await professionalService.deleteProfessionalById(req.params.professionalId);
  response.success(res, 200, professional);
});

const updateProfessional = catchAsync(async (req, res) => {
  const professional = await professionalService.updateProfessionalById(req.params.professionalId, req.body);
  response.success(res, httpStatus.CREATED, professional);
});

const getProfessional = catchAsync(async (req, res) => {
  const post = await professionalService.getProfessionalById(req.params.professionalId);
  response.success(res, 200, post);
});

const checkStatus = catchAsync(async (req, res) => {
  const check = await professionalService.checkProfessional(req.params.professionalId, req.body);
  response.success(res, 200, check);
});

const login = catchAsync(async (req, res) => {
  const login = await professionalService.loginProfessional(req.body);
  const token = await tokenService.generateAuthTokens(login, login.isProfessional);
  response.success(res, 200, { login, token });
});

module.exports = {
  createProfessional,
  getProfessionals,
  deleteProfessional,
  updateProfessional,
  getProfessional,
  checkStatus,
  login,
};
