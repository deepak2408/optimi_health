const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { equipmentsService } = require('../services');
const response = require('../utils/response');

const createEquipments = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const create = await equipmentsService.createEquip(req.body);
  response.success(res, httpStatus.CREATED, create);
});

const getEquipments = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const get = await equipmentsService.getEquip(filter);
  response.success(res, 200, get);
});

const deleteEquipments = catchAsync(async (req, res) => {
  const deleteEquip = await equipmentsService.deleteEquipById(req.params.equipmentsId);
  response.success(res, 200, deleteEquip);
});

const updateEquipments = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return next(response.badRequest('file is empty'));
  }
  const { href } = new URL(`/${req.file.path}`, `${req.baseUrl}`);
  req.body.image = href;
  const update = await equipmentsService.updateEquipById(req.params.equipmentsId, req.body);
  response.success(res, httpStatus.CREATED, update);
});

module.exports = {
    createEquipments,
    getEquipments,
    deleteEquipments,
    updateEquipments,
}