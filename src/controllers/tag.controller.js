const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { tagService } = require('../services');
const response = require('../utils/response');

const createTags = catchAsync(async (req, res) => {
  const createTags = await tagService.createTag(req.body);
  response.success(res, httpStatus.CREATED, createTags);
});

const deleteTags = catchAsync(async (req, res) => {
  const deleteTags = await tagService.deleteTagById(req.params.tagId);
  response.success(res, 200, deleteTags);
});

const updateTags = catchAsync(async (req, res) => {
  const updateTags = await tagService.updateTagById(req.params.tagId, req.body);
  response.success(res, httpStatus.CREATED, updateTags);
});

const getTags = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title']);
  const tags = await tagService.getAllTags(filter);
  response.success(res, 200, tags);
});



module.exports = {
    createTags,
    deleteTags,
    updateTags,
    getTags,
}