const mongoose = require('mongoose');

const partnershipsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        description : {
            type : String,
            required : true,
            unique : true,
        },
        websiteUrl : {
            type : String,
            required : true,
            unique : true,
        },
        affiliationLink : {
            type : String,
            required : true,
            unique : true,
        },
        discount : {
            type : Number,
            required : true,
            unique : true,
        },
        logo : {
            type : String,
            required : true,
            unique : true,
        },

},{
    timestamps : true,
})


const Partnerships = mongoose.model('Partnerships' ,partnershipsSchema);

module.exports = Partnerships;