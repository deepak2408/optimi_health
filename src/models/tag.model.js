const mongoose = require('mongoose');

const tagsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const Tags = mongoose.model('tags' ,tagsSchema);

module.exports = Tags;