const mongoose = require('mongoose');

const adminSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  { collection: 'optimi-admin' },
  {
    timestamps: true,
  }
);

const Admin = mongoose.model('optimi-admin', adminSchema);

module.exports = Admin;
