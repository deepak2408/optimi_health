const mongoose = require('mongoose');

const phaseTestSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const PhaseTest = mongoose.model('PhaseTest' ,phaseTestSchema);

module.exports = PhaseTest;