const mongoose = require('mongoose');

const equipmentsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        image : {
            type : String,
            required: true,
        }
},{
    timestamps : true,
})


const Equipments = mongoose.model('Equipments' ,equipmentsSchema);

module.exports = Equipments;