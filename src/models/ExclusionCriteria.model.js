const mongoose = require('mongoose');

const exclusionCriteriaSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const ExclusionCriteria = mongoose.model('ExclusionCriteria' ,exclusionCriteriaSchema);

module.exports = ExclusionCriteria;