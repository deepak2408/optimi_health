const mongoose = require('mongoose');

const programLevelSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        image:  {
            type : String,
            required: true,
        }
},{
    timestamps : true,
})


const ProgramLevel = mongoose.model('ProgramLevel' ,programLevelSchema);

module.exports = ProgramLevel;