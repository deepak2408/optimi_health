const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const stageDetailsSchema = mongoose.Schema({
  currentStage: {
    type: Number,
  },
  exercises: [{ type: Schema.Types.ObjectId, ref: 'Exercises' }],
  markers: [{ type: Schema.Types.ObjectId, ref: 'Exercises' }],
  stageLevel: { type: Schema.Types.ObjectId, ref: 'ProgramLevel' },
});

const selfManageProgramSchema = mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
    },
    stageCount: {
      type: Number,
    },
    stages: [stageDetailsSchema],
  },
  {
    timestamps: true,
  }
);

selfManageProgramSchema.plugin(paginate);

const SelfManageProgram = mongoose.model('SelfManageProgram', selfManageProgramSchema);

module.exports = SelfManageProgram;
