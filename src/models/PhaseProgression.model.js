const mongoose = require('mongoose');

const phaseProgressionSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const PhaseProgression = mongoose.model('PhaseProgression' ,phaseProgressionSchema);

module.exports = PhaseProgression;