const mongoose = require('mongoose');

const alternativeExerciseSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);


const AlternativeExercise = mongoose.model('AlternativeExercise' ,alternativeExerciseSchema);

module.exports = AlternativeExercise;