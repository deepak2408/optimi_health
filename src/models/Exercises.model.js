const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');


const exercisesSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    bodyParts: [{ type: Schema.Types.ObjectId, ref: 'BodyParts', required: true }],
    focusPoints: [{ type: Schema.Types.ObjectId, ref: 'FocusPoints', required: true }],
    equipments: {
      type: Schema.Types.ObjectId,
      ref: 'Equipments',
      required: true,
      default: null,
    },
    regressionExercise: {
      type: Schema.Types.ObjectId,
      ref: 'RegressionExercise',
      required: true,
    },
    progressionExercise: [{ type: Schema.Types.ObjectId, ref: 'ProgressionExercise', required: true }],
    alternativeExercise: [{ type: Schema.Types.ObjectId, ref: 'ElternativeExercise', required: true }],
    exclusionsCriteria: [{ type: Schema.Types.ObjectId, ref: 'ExclusionCriteria', required: true }],
    level: {
      type: Schema.Types.ObjectId,
      ref: 'ProgramLevel',
      required: true,
    },
    exerciseType: {
      type: Schema.Types.ObjectId,
      ref: 'ExerciseType',
      required: true,
    },
    videoUrl: {
      type: String,
      required: true,
    },
    instruction: {
      type: String,
    },
    tips: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

exercisesSchema.plugin(paginate);


const Exercises = mongoose.model('Exercises' ,exercisesSchema);

module.exports = Exercises;