const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const sessionSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        description : {
            type : String,
            required : true,
        },
        sessionDuration : {
            type : Number,
            required : true,
        },
        restBetweenExercise : {
            type : Number,
            required : true,
        },
        sessionExercises : [{
            exercise : {type : Schema.Types.ObjectId ,ref : 'Exercises'},
            sets : {type : Number, required : true},
            rest : {type : Number, required : true},
            reps : {type : Number, required : true},
            method : {type : Schema.Types.ObjectId , ref : 'Method'},
            bodyParts : {type : Schema.Types.ObjectId , ref : 'BodyParts'},
            equipmentValue : {type : Number , required : true},
        }],
        image : {
            type : String,
            required: true,
        }
},{
    timestamps : true,
})

sessionSchema.plugin(paginate);


const Session = mongoose.model('Session' ,sessionSchema);

module.exports = Session;