const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const phasesSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        description : {
            type : String,
            required : true,
            unique : true,
        },
        phaseProgression : {
            type : Schema.Types.ObjectId , 
            ref : 'PhaseProgression',
            required : true
        },
        phaseGoal : {
            type : Schema.Types.ObjectId , 
            ref : 'ProgramGoal',
            required : true
        },
        sessionInARow : {
            type : Number,
            required : true,
            unique : true,
        },
        phaseTest : {
            type : Schema.Types.ObjectId , 
            ref : 'PhaseTest',
            required : true
        },
        phaseSessions : [{type : Schema.Types.ObjectId , required : true , ref : 'Session'}],
        status : {
            type : String,
            required : true,
            unique : true,
        },
        image : {
            type : String,
            required : true,
            unique : true,
        },


},{
    timestamps : true,
})

phasesSchema.plugin(paginate);


const Phases = mongoose.model('Phases' ,phasesSchema);

module.exports = Phases;