const mongoose = require('mongoose');

const regressionExerciseSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const RegressionExercise = mongoose.model('RegressionExercise' ,regressionExerciseSchema);

module.exports = RegressionExercise;