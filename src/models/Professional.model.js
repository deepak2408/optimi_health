const mongoose = require('mongoose');
const validator = require('validator');

const professionalSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    age: {
      type: Number,
    },
    country: {
      type: String,
    },
    state: {
      type: String,
    },
    city: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    phoneNo: {
      type: String,
      required: true,
    },
    zip: {
      type: Number,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Email  is invalid');
        }
      },
    },
    password: {
      type: String,
      required: true,
    },
    status: {
      type: Boolean,
      default: false,
    },
    isProfessional: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

const Professional = mongoose.model('Professional', professionalSchema);

module.exports = Professional;
