const mongoose = require('mongoose');

const focusPointsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const FocusPoints = mongoose.model('FocusPoints' ,focusPointsSchema);

module.exports = FocusPoints;