const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const forumQuestionSchema = mongoose.Schema(
  {
    title: {
      type: String,
      unique: true,
    },
    description: {
      type: String,
      required: true,
    },
    tag: {
      type: Schema.Types.ObjectId,
      ref: 'Tags',
    },
    featured: {
      type: String,
      required: true,
    },
    status: {
      type: String,
    },
    image: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

forumQuestionSchema.plugin(paginate);

const ForumQuestion = mongoose.model('ForumQuestion', forumQuestionSchema);

module.exports = ForumQuestion;
