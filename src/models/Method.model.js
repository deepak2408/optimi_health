const mongoose = require('mongoose');

const methodSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const Method = mongoose.model('Method' ,methodSchema);

module.exports = Method;