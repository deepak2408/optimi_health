const mongoose = require('mongoose');

const demoUserStatusSchema = mongoose.Schema(
  {
    uniqueId: {
      type: String,
      required: true,
      unique: true,
    },
    status: {
      type: String,
      enum: ['download', 'registered', 'triage', 'plan'],
    },
  },
  {
    timestamps: true,
  }
);


const DemoUserStatus = mongoose.model('DemoUserStatus', demoUserStatusSchema);

module.exports = DemoUserStatus;
