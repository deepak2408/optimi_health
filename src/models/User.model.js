const mongoose = require('mongoose');
const { paginate } = require('./plugins');
const validator = require('validator');

const userSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    weight: {
      type: Number,
    },
    height: {
      type: Number,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Email  is invalid');
        }
      },
    },
    password: {
      type: String,
      required: true,
    },
    isProfessional: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.plugin(paginate);

const User = mongoose.model('User', userSchema);

module.exports = User;
