const mongoose = require('mongoose');

const progressionExerciseSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const ProgressionExercise = mongoose.model('ProgressionExercise' ,progressionExerciseSchema);

module.exports = ProgressionExercise;