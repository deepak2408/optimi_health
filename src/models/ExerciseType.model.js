const mongoose = require('mongoose');

const exerciseTypeSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const ExerciseType = mongoose.model('ExerciseType' ,exerciseTypeSchema);

module.exports = ExerciseType;