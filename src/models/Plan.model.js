const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const planSchema = mongoose.Schema(
  {
    title: {
      type: String,
      unique: true,
    },
    shortDescription: {
      type: String,
      required: true,
    },
    planPrice: {
      type: Number,
      required: true,
    },
    planVideo: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    planProgram: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Program',
      },
    ],
    status: {
      type: String,
    },
    image: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

planSchema.plugin(paginate);

const Plan = mongoose.model('Plan', planSchema);

module.exports = Plan;
