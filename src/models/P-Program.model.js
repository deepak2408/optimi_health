const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const programSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        description : {
            type : String,
            required : true
        },
        programGoal : {
            type : Schema.Types.ObjectId , 
            ref : 'ProgramGoal',
            required : true

        },
        programLevel : {
            type : Schema.Types.ObjectId , 
            ref : 'ProgramLevel',
            required : true
        },
        programPhases : [{type : Schema.Types.ObjectId ,ref : 'Phases' ,
        required : true}],
        status : { 
            type : String,
            required : true,
        },
        image : {
            type : String,
            required : true
        }
},{
    timestamps : true,
})

programSchema.plugin(paginate);


const Program = mongoose.model('Program' ,programSchema);

module.exports = Program;