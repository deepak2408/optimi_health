const mongoose = require('mongoose');

const programPhasesSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const ProgramPhases = mongoose.model('ProgramPhases' ,programPhasesSchema);

module.exports = ProgramPhases;