const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { paginate } = require('./plugins');

const postSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
        },
        description : {
            type : String,
            required : true,
        },
        tag : {
            type : Schema.Types.ObjectId , 
            ref : 'tags',
            required : true
        },
        featured : {
            type : Boolean,
            default : false
        },
        status : {
            type : String,
            required : true
        },
        image : {
            type : String,
            required : true
        }
    }
    ,{
        timestamps: true,
      })


      postSchema.plugin(paginate);


    const Post = mongoose.model('Post', postSchema);

    module.exports = Post;