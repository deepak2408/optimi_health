const mongoose = require('mongoose');

const forumTagsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        }
},{
    timestamps : true,
})


const ForumTags = mongoose.model('ForumTags' ,forumTagsSchema);

module.exports = ForumTags;