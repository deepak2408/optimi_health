const mongoose = require('mongoose');

const bodyPartsSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        image : {
            type : String,
            required: true,
        }
},{
    timestamps : true,
})


const BodyParts = mongoose.model('BodyParts' ,bodyPartsSchema);

module.exports = BodyParts;