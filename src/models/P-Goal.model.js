const mongoose = require('mongoose');

const programGoalSchema = mongoose.Schema(
    {
        title : {
            type : String,
            required : true,
            unique : true,
        },
        image:  {
            type : String,
            required: true,
        }
},{
    timestamps : true,
})


const ProgramGoal = mongoose.model('ProgramGoal' ,programGoalSchema);

module.exports = ProgramGoal;