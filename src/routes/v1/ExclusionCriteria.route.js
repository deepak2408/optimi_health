const express = require('express');
const validate = require('../../middlewares/validate');
const exclusionCriteriaValidation = require('../../validations/ExclusionCriteria.validation');
const exclusionCriteriaController = require('../../controllers/ExclusionCriteria.controller');

const router = express.Router();

router.post('/create' ,validate(exclusionCriteriaValidation.createExclusionCriteria), exclusionCriteriaController.createExclusionCriteria );
router.get('/getAll' ,validate(exclusionCriteriaValidation.getExclusionCriteria), exclusionCriteriaController.getExclusionCriteria);
router.delete('/delete/:exclusionCriteriaId' ,validate(exclusionCriteriaValidation.deleteExclusionCriteria), exclusionCriteriaController.deleteExclusionCriteria);
router.put('/update/:exclusionCriteriaId' ,validate(exclusionCriteriaValidation.updateExclusionCriteria), exclusionCriteriaController.updateExclusionCriteria);

module.exports = router;



/**
 * @swagger
 *  components:
 *    schemas:
 *      ExclusionCriteria:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          exclusionCriteriaId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of exclusionCriteria Exercise
 *        example:
 *           title: title of ExclusionCriteria Exercise
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ExclusionCriteria
 *   description: API to manage ExclusionCriteria .
 */


/**
 * @swagger
 *  /exclusionCriteria/create:
 *     post:
 *      summary: Creates new Exclusion Criteria
 *      tags: [ExclusionCriteria]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExclusionCriteria'
 *      responses:
 *        "200":
 *          description: created ExclusionCriteria.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExclusionCriteria' 
 * 
 */


/**
 * @swagger
 *  /exclusionCriteria/getAll:
 *    get:
 *      summary: List of all ExclusionCriteria
 *      tags: [ExclusionCriteria]
 *      responses:
 *        "200":
 *          description: list of ExclusionCriteria.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AlternativeExercise'
 * 
 */


/**
 * @swagger
 *  /exclusionCriteria/{exclusionCriteriaId}:
 *    delete:
 *      summary: Delete ExclusionCriteria by Id
 *      tags: [ExclusionCriteria]
 *      parameters:
 *        - in: path
 *          name: exclusionCriteriaId
 *          schema:
 *            type: string
 *          required: true
 *          description: exclusionCriteria objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ExclusionCriteria Id not found.
 */


/**
 * @swagger
 *  /exclusionCriteria/{exclusionCriteriaId}:
 *  put:
 *      summary: Update ExclusionCriteria
 *      tags: [ExclusionCriteria]
 *      parameters:
 *        - in: path
 *          name: exclusionCriteriaId
 *          schema:
 *            type: string
 *          required: true
 *          description: ExclusionCriteria objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExclusionCriteria'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: ExclusionCriteria not found.
 */

