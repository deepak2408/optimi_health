const express = require('express');
const postController = require('../../controllers/post.controller');
const { upload } = require('../../middlewares/upload');
const postValidation = require('../../validations/post.validation');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post('/create', upload.single('image'), validate(postValidation.createPosts), baseUrl, postController.uploadPic);
router.get('/getAll', validate(postValidation.getPosts), postController.getPosts);
router.get('/get/:postId', validate(postValidation.getPost), postController.getPost);
router.delete('/delete/:postId', validate(postValidation.deletePosts), postController.deletePosts);
router.put(
  '/update/:postId',
  upload.single('image'),
  validate(postValidation.updatePosts),
  baseUrl,
  postController.updatePosts
);


module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Post:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - tag
 *          - featured
 *          - status
 *          - image
 *        properties:
 *          postId:
 *            type: string
 *          title:
 *            type: string
 *            description: Post title
 *          description:
 *            type: string
 *            description: description about post.
 *          tag:
 *            type: string
 *            description: ObjectId of tag. 
 *          featured:
 *            type: boolean
 *            description: give featured in boolean.
 *          status:
 *            type: string
 *            description: status about Post.
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of post
 *           description: description about post
 *           tag: 623c7a4156f148136cfcdc69
 *           featured: post feature
 *           status: post status
 *           image: image of post in jpg,png format
 *           
 */

/**
 * @swagger
 *  /post/create:
 *  post:
 *      summary: Create post
 *      tags: [Post]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *             $ref: '#/components/schemas/Post'
 *      responses:
 *        "200":
 *          description: created Post.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Post'
 * 
 */


/**
 * @swagger
 *  /post/getAll:
 *    get:
 *      summary: List of all Post
 *      tags: [Post]
 *      responses:
 *        "200":
 *          description: list of Post.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Post'
 * 
 */


/**
 * @swagger
 *  /post/{postId}:
 *    get:
 *      summary: Get Post by Id
 *      tags: [Post]
 *      parameters:
 *        - in: path
 *          name: postId
 *          schema:
 *            type: string
 *          required: true
 *          description: Post objectId
 *      responses:
 *        "200":
 *          description: list of post.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Post'
 *        "404":
 *          description: Post not found.
 */


/**
 * @swagger
 *  /post/{postId}:
 *    delete:
 *      summary: Delete post by Id
 *      tags: [Post]
 *      perameters:
 *        - in: path
 *          name: postId
 *          schema:
 *            type: string
 *          required: true
 *          description: Post objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Post Id not found.
 */


/**
 * @swagger
 *  /post/{postId}:
 *  put:
 *      summary: Update post
 *      tags: [Post]
 *      parameters:
 *        - in: path
 *          name: postId
 *          schema:
 *            type: string
 *          required: true
 *          description: Post objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Post'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Post not found.
 */


