const express = require('express');
const validate = require('../../middlewares/validate');
const { upload } = require('../../middlewares/upload');
const programLevelValidation = require('../../validations/P-Level.validation');
const programLevelController = require('../../controllers/P-Level.controller');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(programLevelValidation.createProgramLevel),
  baseUrl,
  programLevelController.createProgramLevel
);
router.get('/getAll' ,validate(programLevelValidation.getProgramLevel), programLevelController.getProgramLevel);
router.delete('/delete/:programLevelId' ,validate(programLevelValidation.deleteProgramLevel), programLevelController.deleteProgramLevel);
router.put(
  '/update/:programLevelId',
  upload.single('image'),
  validate(programLevelValidation.updateProgramLevel),
  baseUrl,
  programLevelController.updateProgramLevel
);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      ProgramLevel:
 *        type: object
 *        required:
 *          - title
 *          - image
 *        properties:
 *          programLevelId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of ProgramLevel
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of programLevel
 *           image: image  of programLevel in jpg,png format
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ProgramLevel
 *   description: API to manage ProgramLevel.
 */


/**
 * @swagger
 *  /programLevel/create:
 *     post:
 *      summary: Creates new ProgramLevel
 *      tags: [ProgramLevel]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/ProgramLevel'
 *      responses:
 *        "200":
 *          description: created ProgramLevel.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgramLevel' 
 * 
 */


/**
 * @swagger
 *  /programLevel/getAll:
 *    get:
 *      summary: List of all ProgramLevel
 *      tags: [ProgramLevel]
 *      responses:
 *        "200":
 *          description: list of ProgramLevel.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgramLevel'
 * 
 */


/**
 * @swagger
 *  /programLevel/{programLevelId}:
 *    delete:
 *      summary: Delete programLevel by Id
 *      tags: [ProgramLevel]
 *      parameters:
 *        - in: path
 *          name: programLevelId
 *          schema:
 *            type: string
 *          required: true
 *          description: ProgramLevel objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ProgramLevel Id not found.
 */


/**
 * @swagger
 *  /programLevel/{programLevelId}:
 *  put:
 *      summary: Update ProgramLevel
 *      tags: [ProgramLevel]
 *      parameters:
 *        - in: path
 *          name: programLevelId
 *          schema:
 *            type: string
 *          required: true
 *          description: programLevel objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/ProgramLevel'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: ProgramLevel not found.
 */

