const express = require('express');
const postRoute = require('./post.route');
const tagRoute = require('./tag.route');
const docsRoute = require('./docs.route');
const config = require('../../config/config');
const programGoalRoute = require('./P-Goal.route');
const programLevelRoute = require('./P-Level.route');
const programPhasesRoute = require('./P-Phases.route');
const programRoute = require('./P-Program.route');
const bodyParts = require('./BodyParts.route');
const focusPoints = require('./FocusPoints.route');
const equipments = require('./Equipments.route');
const exerciseType = require('./ExerciseType.route');
const exclusionCriteria = require('./ExclusionCriteria.route');
const regressionExercise = require('./RegressionExercise.route');
const progressionExercise = require('./ProgressionExercise.route');
const alternativeExercise = require('./AlternativeExercise.route');
const exercises = require('./Exercises.route');
const method = require('./Method.route');
const session = require('./Session.route');
const partnerships = require('./Partnerships.route');
const forumTags = require('./ForumTags.route');
const phases = require('./Phases.route');
const phaseTest = require('./PhaseTest.route');
const phaseProgression = require('./PhaseProgression.route');
const professional = require('./Professional.route');
const admin = require('./optimi-admin.route');
const forumQuestion = require('./ForumQuestions.route');
const plan = require('./Plan.route');
const selfManageProgram = require('./SelfManageProgram.route');
const user = require('./User.route');
const demoUserStatus = require('./Demo-UserStatus.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/post',
    route: postRoute,
  },
  {
    path: '/tag',
    route: tagRoute,
  },
  {
    path: '/programGoal',
    route: programGoalRoute,
  },
  {
    path: '/programLevel',
    route: programLevelRoute,
  },
  {
    path: '/programPhases',
    route: programPhasesRoute,
  },
  {
    path: '/program',
    route: programRoute,
  },
  {
    path: '/bodyParts',
    route: bodyParts,
  },
  {
    path: '/focusPoints',
    route: focusPoints,
  },
  {
    path: '/equipments',
    route: equipments,
  },
  {
    path: '/exerciseType',
    route: exerciseType,
  },
  {
    path: '/exclusionCriteria',
    route: exclusionCriteria,
  },
  {
    path: '/regressionExercise',
    route: regressionExercise,
  },
  {
    path: '/progressionExercise',
    route: progressionExercise,
  },
  {
    path: '/alternativeExercise',
    route: alternativeExercise,
  },
  {
    path: '/exercises',
    route: exercises,
  },
  {
    path: '/method',
    route: method,
  },
  {
    path: '/session',
    route: session,
  },
  {
    path: '/partnerships',
    route: partnerships,
  },
  {
    path: '/forumTags',
    route: forumTags,
  },
  {
    path: '/phases',
    route: phases,
  },
  {
    path: '/phaseTest',
    route: phaseTest,
  },
  {
    path: '/phaseProgression',
    route: phaseProgression,
  },
  {
    path: '/professional',
    route: professional,
  },
  {
    path: '/admin',
    route: admin,
  },
  {
    path: '/forumQuestion',
    route: forumQuestion,
  },
  {
    path: '/plan',
    route: plan,
  },
  {
    path: '/selfManageProgram',
    route: selfManageProgram,
  },
  {
    path: '/user',
    route: user,
  },
  {
    path: '/demoUserStatus',
    route: demoUserStatus,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
