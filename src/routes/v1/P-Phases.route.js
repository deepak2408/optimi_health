const express = require('express');
const validate = require('../../middlewares/validate');
const programPhasesValidation = require('../../validations/P-Phases.validation');
const programPhasesController = require('../../controllers/P-Phases.controller');

const router = express.Router();

router.post('/create' ,validate(programPhasesValidation.createProgramPhases), programPhasesController.createProgramPhases );
router.get('/getAll' ,validate(programPhasesValidation.getProgramPhases), programPhasesController.getProgramPhases);
router.delete('/delete/:programPhaseId' ,validate(programPhasesValidation.deleteProgramPhases), programPhasesController.deleteProgramPhases);
router.put('/update/:programPhaseId' ,validate(programPhasesValidation.updateProgramPhases), programPhasesController.updateProgramPhases);

module.exports = router;