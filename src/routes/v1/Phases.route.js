const express = require('express');
const phasesController = require('../../controllers/Phases.controller');
const { upload } = require('../../middlewares/upload');
const phasesValidation = require('../../validations/Phases.validation');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');


const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(phasesValidation.createPhases),
  baseUrl,
  phasesController.createPhases
);
router.get('/getAll', validate(phasesValidation.getPhases), phasesController.getPhases);
router.get('/get/:phaseId', validate(phasesValidation.getPhase), phasesController.getPhase);
router.delete('/delete/:phaseId', validate(phasesValidation.deletePhases), phasesController.deletePhases);
router.put(
  '/update/:phaseId',
  upload.single('image'),
  validate(phasesValidation.updatePhases),
  baseUrl,
  phasesController.updatePhases
);



module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Phases:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - phaseProgression 
 *          - phaseGoal
 *          - sessionInARow
 *          - phaseTest
 *          - phaseSessions
 *          - status
 *          - image
 *        properties:
 *          phaseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of Phases
 *          description:
 *            type: string
 *            description: description of phases. 
 *          phaseProgression:
 *            type: string
 *            description: ObjectId of phaseProgression.
 *          phaseGoal:
 *            type: string
 *            description: ObjectId of phaseGoal.
 *          sessionInARow:
 *            type: number
 *            description: numbr of session in a row.
 *          phaseTest:
 *            type: string
 *            description: ObjectId of phaseTest.
 *          phaseSessions:
 *            type: array
 *            description: ObjectId of phaseSessions.
 *          status:
 *            type: string
 *            description: status of Phases.
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Phases
 *           description: description of phases
 *           phaseProgression: 623c7a4156f148136cfcdc69
 *           phaseGoal: 623c7a4156f148136cfcdc69
 *           sessionInARow: number(3)
 *           phaseTest: 623c7a4156f148136cfcdc69
 *           phaseSessions: [623c7a4156f148136cfcdc69]
 *           status: stutus of phases
 *           image: image  of phases in jpg,png format
 *           
 */

/**
 * @swagger
 * tags:
 *   name: Phases
 *   description: API to manage Phases.
 */



/**
 * @swagger
 *  /phases/create:
 *     post:
 *      summary: Creates new Phases
 *      tags: [Phases]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Phases'
 *      responses:
 *        "200":
 *          description: created Phases.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Phases' 
 * 
 */


/**
 * @swagger
 *  /phases/getAll:
 *    get:
 *      summary: List of all Phases
 *      tags: [Phases]
 *      responses:
 *        "200":
 *          description: list of Phases.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Phases'
 * 
 */


/**
 * @swagger
 *  /phases/{phasesId}:
 *    get:
 *      summary: Get Phase by Id
 *      tags: [Phases]
 *      parameters:
 *        - in: path
 *          name: phaseId
 *          schema:
 *            type: string
 *          required: true
 *          description: Phases objectId
 *      responses:
 *        "200":
 *          description: list of phases.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Phases'
 *        "404":
 *          description: Phases not found.
 */

/**
 * @swagger
 *  /phases/{phasesId}:
 *  put:
 *      summary: Update Phases
 *      tags: [Phases]
 *      parameters:
 *        - in: path
 *          name: phasesId
 *          schema:
 *            type: string
 *          required: true
 *          description: Phases objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Phases'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Phases not found.
 */

/**
 * @swagger
 *  /phases/{phasesId}:
 *    delete:
 *      summary: Delete phase by Id
 *      tags: [Phases]
 *      parameters:
 *        - in: path
 *          name: phaseId
 *          schema:
 *            type: string
 *          required: true
 *          description: Phase objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Phase Id not found.
 */

     