const express = require('express');
const exerciseController = require('../../controllers/Exercises.controller');
const { upload } = require('../../middlewares/upload');
const exerciseValidation = require('../../validations/Exercises.validation');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(exerciseValidation.createExercise),
  baseUrl,
  exerciseController.createExercise
);
router.get('/getAll', validate(exerciseValidation.getExercises), exerciseController.getExercises);
router.get('/get/:exerciseId', validate(exerciseValidation.getExercise), exerciseController.getExercise);
router.delete('/delete/:exerciseId', validate(exerciseValidation.deleteExercise), exerciseController.deleteExercise);
router.put(
  '/update/:exerciseId',
  upload.single('image'),
  validate(exerciseValidation.updateExercise),
  baseUrl,
  exerciseController.updateExercise
);



module.exports = router;




/**
 * @swagger
 *  components:
 *    schemas:
 *      Exercises:
 *        type: object
 *        required:
 *          - title
 *          - bodyParts
 *          - focusPoints
 *          - equipments
 *          - regressionExercise
 *          - progressionExercise
 *          - alternativeExercise
 *          - enclusionsCriteria
 *          - level
 *          - exerciseType
 *          - videoUrl
 *          - instruction
 *          - tips
 *          - image
 *        properties:
 *          exerciseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of exercise
 *          bodyParts:
 *            type: array
 *            description: ObjectId of BodyParts. 
 *          focusPoints:
 *            type: array
 *            description: ObjectId of focusPoints.
 *          equipments:
 *            type: string
 *            description: ObjectId of equipments.
 *          regressionExercise:
 *            type: string
 *            description: ObjectId of regressionExercise.
 *          progressionExercise:
 *            type: array
 *            description: ObjectId of progressionExercise.
 *          alternativeExercise:
 *            type: array
 *            description: ObjectId of alternativeExercise.
 *          enclusionsCriteria:
 *            type: array
 *            description: ObjectId of enclusionsCriteria.
 *          level:
 *            type: string
 *            description: ObjectId of level.
 *          exerciseType:
 *            type: string
 *            description: ObjectId of exerciseType.
 *          videoUrl:
 *            type: string
 *            description: give videoUrl.
 *          instruction:
 *            type: string
 *            description: instruction about exercise
 *          tips:
 *            type: string
 *            description: tips about exercise
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of exercise
 *           bodyParts: [623c7a4156f148136cfcdc69]
 *           focusPoints: [623c7a4156f148136cfcdc69]
 *           equipments: 623c7a4156f148136cfcdc69
 *           regressionExercise: 623c7a4156f148136cfcdc69
 *           progressionExercise: [623c7a4156f148136cfcdc69]
 *           alternativeExercise: [623c7a4156f148136cfcdc69]
 *           enclusionsCriteria: [623c7a4156f148136cfcdc69]
 *           level: 623c7a4156f148136cfcdc69
 *           exerciseType: 623c7a4156f148136cfcdc69
 *           videoUrl: url of exercise
 *           instruction: instruction about exercise
 *           tips: tips about exercise
 *           image: image  of exercise in jpg,png format
 *           
 * 
 */


/**
 * @swagger
 * tags:
 *   name: Exercises
 *   description: API to manage Exercise.
 */


/**
 * @swagger
 *  /exercises/create:
 *     post:
 *      summary: Creates new exercise
 *      tags: [Exercises]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Exercises'
 *      responses:
 *        "200":
 *          description: created Exercises.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Exercises' 
 * 
 */


/**
 * @swagger
 *  /exercises/getAll:
 *    get:
 *      summary: List of all Exercises
 *      tags: [Exercises]
 *      responses:
 *        "200":
 *          description: list of Exercises.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Exercises'
 * 
 */

/**
 * @swagger
 *  /exercises/{exerciseId}:
 *    get:
 *      summary: Get Exercise by Id
 *      tags: [Exercises]
 *      parameters:
 *        - in: path
 *          name: exerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: Exercise objectId
 *      responses:
 *        "200":
 *          description: list of exercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Exercises'
 *        "404":
 *          description: Exercise not found.
 */

/**
 * @swagger
 *  /exercises/{exerciseId}:
 *  put:
 *      summary: Update exercise
 *      tags: [Exercises]
 *      parameters:
 *        - in: path
 *          name: exerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: Exercise objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Session'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Session not found.
 */

/**
 * @swagger
 *  /exercises/{exerciseId}:
 *    delete:
 *      summary: Delete exercise by Id
 *      tags: [Exercises]
 *      parameters:
 *        - in: path
 *          name: exerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: Exercise objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Exercise Id not found.
 */
