const express = require('express');
const validate = require('../../middlewares/validate');
const { demoUserStatusValidation } = require('../../validations');
const { demoUserStatusController } = require('../../controllers');

const router = express.Router();

router.post('/create', validate(demoUserStatusValidation.createDemoUser), demoUserStatusController.createDemoUser);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      DemoUserStatus:
 *        type: object
 *        required:
 *          - uniqueId
 *          - status
 *        properties:
 *          demoId:
 *            type: string
 *          uniqueId:
 *            type: string
 *            required: true
 *          status:
 *            type: string
 *            enum: [userAppDownload,registered,triage,plan]
 *        example:
 *           uniqueId: uniqueId
 *           status: plan 
 *           
 */

/**
 * @swagger
 * tags:
 *   name: DemoUserStatus
 *   description: API to manage DemoUserStatus.
 */

/**
 * @swagger
 *  /demoUserStatus/create:
 *     post:
 *      summary: Creates demoUserStatus
 *      tags: [DemoUserStatus]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/DemoUserStatus'
 *      responses:
 *        "200":
 *          description: created DemoStatus.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/DemoUserStatus' 
 * 
 */

