const express = require('express');
const { planController } = require('../../controllers');
const { upload } = require('../../middlewares/upload');
const { planValidation } = require('../../validations');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post('/create', upload.single('image'), validate(planValidation.createPlan), baseUrl, planController.createPlan);
router.get('/getAll', validate(planValidation.getPlans), planController.getPlans);
router.get('/get/:planId', validate(planValidation.getPlan), planController.getPlan);
router.delete('/delete/:planId', validate(planValidation.deletePlan), planController.deletePlan);
router.put(
  '/update/:planId',
  upload.single('image'),
  validate(planValidation.updatePlan),
  baseUrl,
  planController.updatePlan
);

module.exports = router;

/**
 * @swagger
 *  components:
 *    schemas:
 *      Plan:
 *        type: object
 *        required:
 *          - title
 *          - shortDescription
 *          - planPrice
 *          - planVideo
 *          - description
 *          - planProgram
 *          - status
 *          - image
 *        properties:
 *          planId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of Plan.
 *          shortDescription:
 *            type: string
 *            description: short description of Plan.
 *          planPrice:
 *            type: number
 *            description: planPrice of Plan.
 *          planVideo:
 *            type: string
 *            description: planVideo title.
 *          description:
 *            type: string
 *            description: description about Plan.
 *          planProgram:
 *            type: array
 *            description: objectId of planProgram.
 *          status:
 *            type: string
 *            description: status of Plan.
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Plan
 *           shortDescription: short description of Plan
 *           planPrice: 340
 *           planVideo: title of PlanVideo
 *           description: description about plan
 *           planProgram: [623c7a4156f148136cfcdc69]
 *           status: stutus of Plan
 *           image: image in jpg,png format
 *
 */

/**
 * @swagger
 * tags:
 *   name: Plan
 *   description: API to manage Plan.
 */

/**
 * @swagger
 *  /plan/create:
 *     post:
 *      summary: Creates new Plan
 *      tags: [Plan]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Plan'
 *      responses:
 *        "200":
 *          description: created Plan.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Plan'
 *
 */

/**
 * @swagger
 *  /plan/getAll:
 *    get:
 *      summary: List of all Plan
 *      tags: [Plan]
 *      responses:
 *        "200":
 *          description: list of Plans.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Plan'
 *
 */

/**
 * @swagger
 *  /plan/{planId}:
 *    get:
 *      summary: Get Plan by Id
 *      tags: [Plan]
 *      parameters:
 *        - in: path
 *          name: planId
 *          schema:
 *            type: string
 *          required: true
 *          description: Plan objectId
 *      responses:
 *        "200":
 *          description: list of Plan.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Plan'
 *        "404":
 *          description: ForumQuestion not found.
 */

/**
 * @swagger
 *  /plan/{planId}:
 *  put:
 *      summary: Update Plan
 *      tags: [Plan]
 *      parameters:
 *        - in: path
 *          name: planId
 *          schema:
 *            type: string
 *          required: true
 *          description: plan objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Plan'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Plan not found.
 */

/**
 * @swagger
 *  /plan/{PlanId}:
 *    delete:
 *      summary: Delete plan by Id
 *      tags: [Plan]
 *      parameters:
 *        - in: path
 *          name: planId
 *          schema:
 *            type: string
 *          required: true
 *          description: plan objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Plan Id not found.
 */
