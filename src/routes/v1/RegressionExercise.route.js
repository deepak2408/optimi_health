const express = require('express');
const validate = require('../../middlewares/validate');
const regressionExerciseValidation = require('../../validations/RegressionExercise.validation');
const regressionExerciseController = require('../../controllers/RegressionExercise.controller');

const router = express.Router();

router.post('/create' ,validate(regressionExerciseValidation.createRegressionExercise), regressionExerciseController.createRegressionExercise );
router.get('/getAll' ,validate(regressionExerciseValidation.getRegressionExercise), regressionExerciseController.getRegressionExercise);
router.delete('/delete/:regressionExerciseId' ,validate(regressionExerciseValidation.deleteRegressionExercise), regressionExerciseController.deleteRegressionExercise);
router.put('/update/:regressionExerciseId' ,validate(regressionExerciseValidation.updateRegressionExercise), regressionExerciseController.updateRegressionExercise);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      RegressionExercise:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          regressionExerciseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of regressionExercise
 *        example:
 *           title: title of regressionExercsie
 *           
 */


/**
 * @swagger
 * tags:
 *   name: RegressionExercise
 *   description: API to manage RegressionExecise .
 */


/**
 * @swagger
 *  /regressionExercise/create:
 *     post:
 *      summary: Creates new RegressionExercise
 *      tags: [RegressionExercise]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/RegressionExercise'
 *      responses:
 *        "200":
 *          description: created RegressionExercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/RegressionExercise' 
 * 
 */


/**
 * @swagger
 *  /regressionExercise/getAll:
 *    get:
 *      summary: List of all RegressionExercise
 *      tags: [RegressionExercise]
 *      responses:
 *        "200":
 *          description: list of RegressionExercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/RegressionExercise'
 * 
 */


/**
 * @swagger
 *  /regressionExercise/{regressionExerciseId}:
 *    delete:
 *      summary: Delete regressionExercise by Id
 *      tags: [RegressionExercise]
 *      parameters:
 *        - in: path
 *          name: regressionExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description:  regressionExercise objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: regressionExercise Id not found.
 */


/**
 * @swagger
 *  /regressionExercise/{regressionExerciseId}:
 *  put:
 *      summary: Update regressionExercise by Id
 *      tags: [RegressionExercise]
 *      parameters:
 *        - in: path
 *          name: regressionExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: regressionExercise objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/RegressionExercise'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: regressionExercise not found.
 */

