const express = require('express');
const sessionController = require('../../controllers/Session.controller');
const { upload } = require('../../middlewares/upload');
const sessionValidation = require('../../validations/Session.validation');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');


const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(sessionValidation.createSession),
  baseUrl,
  sessionController.createSession
);
router.get('/getAll', validate(sessionValidation.getSessions), sessionController.getSessions);
router.get('/get/:sessionId', validate(sessionValidation.getSession), sessionController.getSession);
router.delete('/delete/:sessionId', validate(sessionValidation.deleteSession), sessionController.deleteSession);
router.put(
  '/update/:sessionId',
  upload.single('image'),
  validate(sessionValidation.updateSession),
  baseUrl,
  sessionController.updateSession
);



module.exports = router;



/**
 * @swagger
 *  components:
 *    schemas:
 *      Session:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - sessionDuration
 *          - restBetweenExercise
 *          - sessionExercises
 *          - image
 *        properties:
 *          exerciseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of session
 *          description:
 *            type: string
 *            description: description about session . 
 *          sessionDuration:
 *            type: number
 *            description: session duration time.
 *          restBetweenExercise:
 *            type: number
 *            description: rest between exercise.
 *          sessionExercises:
 *            type: array
 *            exercise:
 *               type: string
 *               description: type of exercise
 *            set:
 *               type: number
 *               description: number of sets
 *            rest:
 *               type: number
 *               description: rest time in seconds
 *            reps:
 *               type: number
 *               description: number of reps
 *            method:
 *               type: string
 *               description: object id of method
 *            bodyParts:
 *               type: string
 *               description: object id of bodyParts
 *            equipmentValue:
 *               type: number
 *               description: number of weight of equipment
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of exercise
 *           description: discription about session
 *           sessionDuration: session duration time
 *           restBetweenExercise: 2 sec
 *           sessionExercises: [
 *                     exercise: 623c7a4156f148136cfcdc69,
 *                     sets: 3,
 *                     rest: 20,
 *                     reps: 6,
 *                     method: 623c7a4156f148136cfcdc69,
 *                     bodyParts: 623c7a4156f148136cfcdc69,
 *                     equipmentValue: 32]
 *           image: image  of exercise in jpg,png format
 *           
 * 
 */


/**
 * @swagger
 * tags:
 *   name: Session
 *   description: API to manage Session.
 */

/**
 * @swagger
 *  /session/create:
 *     post:
 *      summary: Creates new session
 *      tags: [Session]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Session'
 *      responses:
 *        "200":
 *          description: created Session.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Session' 
 * 
 */


/**
 * @swagger
 *  /session/getAll:
 *    get:
 *      summary: List of all Session
 *      tags: [Session]
 *      responses:
 *        "200":
 *          description: list of Session.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Session'
 * 
 */


/**
 * @swagger
 *  /session/{sessionId}:
 *    get:
 *      summary: Get Session by Id
 *      tags: [Session]
 *      parameters:
 *        - in: path
 *          name: sessionId
 *          schema:
 *            type: string
 *          required: true
 *          description: Session objectId
 *      responses:
 *        "200":
 *          description: list of session.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Session'
 *        "404":
 *          description: session not found.
 */


/**
 * @swagger
 *  /session/{sessionId}:
 *  put:
 *      summary: Update session
 *      tags: [Session]
 *      parameters:
 *        - in: path
 *          name: sessionId
 *          schema:
 *            type: string
 *          required: true
 *          description: Session objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Exercises'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Exercise not found.
 */


/**
 * @swagger
 *  /session/{sessionId}:
 *    delete:
 *      summary: Delete session by Id
 *      tags: [Session]
 *      parameters:
 *        - in: path
 *          name: sessionId
 *          schema:
 *            type: string
 *          required: true
 *          description: Session objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Session Id not found.
 */



     