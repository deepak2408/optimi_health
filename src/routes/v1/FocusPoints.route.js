const express = require('express');
const validate = require('../../middlewares/validate');
const focusPointsValidation = require('../../validations/FocusPoints.validation');
const focusPointsController = require('../../controllers/FocusPoints.controller');

const router = express.Router();

router.post('/create' ,validate(focusPointsValidation.createFocusPoints), focusPointsController.createFocusPoints );
router.get('/getAll' ,validate(focusPointsValidation.getFocusPoints), focusPointsController.getFocusPoints);
router.delete('/delete/:focusPointsId' ,validate(focusPointsValidation.deleteFocusPoints), focusPointsController.deleteFocusPoints);
router.put('/update/:focusPointsId' ,validate(focusPointsValidation.updateFocusPoints), focusPointsController.updateFocusPoints);

module.exports = router;

/**
 * @swagger
 *  components:
 *    schemas:
 *      FocusPoints:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          focusPointsId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of focusPoints
 *        example:
 *           title: title of focus Points
 *           
 */


/**
 * @swagger
 * tags:
 *   name: FocusPoints
 *   description: API to manage FocusPoints .
 */


/**
 * @swagger
 *  /focusPoints/create:
 *     post:
 *      summary: Creates new FocusPoints
 *      tags: [FocusPoints]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FocusPoints'
 *      responses:
 *        "200":
 *          description: created FocusPoints.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/FocusPoints' 
 * 
 */


/**
 * @swagger
 *  /focusPoints/getAll:
 *    get:
 *      summary: List of all focusPoints
 *      tags: [FocusPoints]
 *      responses:
 *        "200":
 *          description: list of FocusPoints.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/FocusPoints'
 * 
 */


/**
 * @swagger
 *  /focusPoints/{focusPointsId}:
 *    delete:
 *      summary: Delete FocusPoints by Id
 *      tags: [FocusPoints]
 *      parameters:
 *        - in: path
 *          name: focusPointsId
 *          schema:
 *            type: string
 *          required: true
 *          description:  focusPoints objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: FocusPoints Id not found.
 */


/**
 * @swagger
 *  /focusPoints/{focusPointsId}:
 *  put:
 *      summary: Update FocusPoints by Id
 *      tags: [FocusPoints]
 *      parameters:
 *        - in: path
 *          name: focusPointsId
 *          schema:
 *            type: string
 *          required: true
 *          description: focusPoints objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FocusPoints'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: focusPoints not found.
 */

