const express = require('express');
const validate = require('../../middlewares/validate');
const adminValidation = require('../../validations/optimi-admin.validation');
const adminController = require('../../controllers/optimi-admin.controller');

const router = express.Router();

router.post('/login', validate(adminValidation.login), adminController.login);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Admin:
 *        type: object
 *        required:
 *          - username
 *          - password
 *        properties:
 *          username:
 *            type: string
 *            description: username of Admin.
 *          password:
 *            type: string
 *            description: password of Admin. 
 *        example:
 *           username: username
 *           password: password@34
 *           
 */

/**
 * @swagger
 * tags:
 *   name: Admin
 *   description: API to login Admin.
 */



/**
 * @swagger
 *  /admin/login:
 *     post:
 *      summary: Admin Login
 *      tags: [Admin]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Admin'
 *      responses:
 *        "200":
 *          description: Admin login
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Admin' 
 * 
 */

