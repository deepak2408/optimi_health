const express = require('express');
const validate = require('../../middlewares/validate');
const  phaseTestValidation = require('../../validations/PhaseTest.validation');
const phaseTestController = require('../../controllers/PhaseTest.controller');


const router = express.Router();

router.post('/create' ,validate(phaseTestValidation.createPhaseTest), phaseTestController.createPhaseTest );
router.get('/getAll' ,validate(phaseTestValidation.getPhaseTest), phaseTestController.getPhaseTest);
router.delete('/delete/:phaseTestId' ,validate(phaseTestValidation.deletePhaseTest), phaseTestController.deletePhaseTest);
router.put('/update/:phaseTestId' ,validate(phaseTestValidation.updatePhaseTest), phaseTestController.updatePhaseTest);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      PhaseTest:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          phaseTestId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of phaseTest
 *        example:
 *           title: title of phaseTest
 *           
 */


/**
 * @swagger
 * tags:
 *   name: PhaseTest
 *   description: API to manage PhaseTest .
 */


/**
 * @swagger
 *  /phaseTest/create:
 *     post:
 *      summary: Creates new PhaseTest
 *      tags: [PhaseTest]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PhaseTest'
 *      responses:
 *        "200":
 *          description: created PhaseTest.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/PhaseTest' 
 * 
 */


/**
 * @swagger
 *  /phaseTest/getAll:
 *    get:
 *      summary: List of all phaseTest
 *      tags: [PhaseTest]
 *      responses:
 *        "200":
 *          description: list of PhaseTest.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/PhaseTest'
 * 
 */


/**
 * @swagger
 *  /phaseTest/{phaseTestId}:
 *    delete:
 *      summary: Delete PhaseTest by Id
 *      tags: [PhaseTest]
 *      parameters:
 *        - in: path
 *          name: PhaseTestId
 *          schema:
 *            type: string
 *          required: true
 *          description:  phaseTest objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: phaseTest Id not found.
 */


/**
 * @swagger
 *  /phaseTest/{phaseTestId}:
 *  put:
 *      summary: Update phaseTest by Id
 *      tags: [PhaseTest]
 *      parameters:
 *        - in: path
 *          name: phaseTestId
 *          schema:
 *            type: string
 *          required: true
 *          description: phaseTest objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PhaseTest'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: phaseTest not found.
 */

