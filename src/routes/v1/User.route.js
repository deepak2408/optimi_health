const express = require('express');
const validate = require('../../middlewares/validate');
const { userValidation } = require('../../validations');
const { userController } = require('../../controllers');

const router = express.Router();

router.post('/create', validate(userValidation.createUser), userController.createUser);
router.get('/getAll', validate(userValidation.getUser), userController.getUser);
router.delete('/delete/:userId', validate(userValidation.deleteUser), userController.deleteUser);
router.post('/login', validate(userValidation.login), userController.login);

module.exports = router;

/**
 * @swagger
 *  components:
 *    schemas:
 *      User:
 *        type: object
 *        required:
 *          - firstName
 *          - lastName
 *          - age
 *          - weight
 *          - email
 *          - password
 *        properties:
 *          userId:
 *            type: string
 *          firstName:
 *            type: string
 *            description: lastname of User.
 *          lastName:
 *            type: string
 *            description: lastName of user.
 *          age:
 *            type: number
 *            description: age of user.
 *          weight:
 *            type: number
 *            description: weight of user.
 *          email:
 *            type: string
 *            description: email of user.
 *          password:
 *            type: string
 *            description: password of user.
 *        example:
 *           firstName: firstName
 *           lastName: lastName
 *           age: 32
 *           weight: 64
 *           email: re@gmail.com
 *           password: password@1
 *
 */

/**
 * @swagger
 *  /user/create:
 *  post:
 *      summary: Create user
 *      tags: [User]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *             $ref: '#/components/schemas/User'
 *      responses:
 *        "200":
 *          description: created user.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *
 */

/**
 * @swagger
 *  /user/getAll:
 *    get:
 *      summary: List of user
 *      tags: [User]
 *      responses:
 *        "200":
 *          description: list of User.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *
 */

/**
 * @swagger
 *  /user/{userId}:
 *    delete:
 *      summary: Delete user by Id
 *      tags: [User]
 *      perameters:
 *        - in: path
 *          name: userId
 *          schema:
 *            type: string
 *          required: true
 *          description: User objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: User Id not found.
 */
