const express = require('express');
const validate = require('../../middlewares/validate');
const  partnershipsValidation = require('../../validations/Partnerships.validation');
const partnershipsController = require('../../controllers/Partnerships.controller');


const router = express.Router();

router.post('/create' ,validate(partnershipsValidation.createPartnerships), partnershipsController.createPartnerships );
router.get('/getAll' ,validate(partnershipsValidation.getPartnerships), partnershipsController.getPartnerships);
router.delete('/delete/:partnershipId' ,validate(partnershipsValidation.deletePartnerships), partnershipsController.deletePartnerships);
router.put('/update/:partnershipId' ,validate(partnershipsValidation.updatePartnerships), partnershipsController.updatePartnerships);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Partnership:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - websiteUrl
 *          - affiliationLink
 *          - discount
 *          - logo
 *        properties:
 *          partnershipId:
 *            type: string
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          websiteUrl:
 *            type: string
 *          affiliationLink:
 *            type: string
 *          discount:
 *            type: number
 *          logo:
 *            type: string
 *        example:
 *           title: title of partnership
 *           description: description of partnership
 *           websiteUrl: Url you to give
 *           affiliationLink: affiliationLink you to give
 *           discount: discount to give
 *           logo: image logo
 *           
 * 
 */

/**
 * @swagger
 * tags:
 *   name: Partnership
 *   description: API to manage partnership.
 */

/**
 * @swagger
 *  /partnerships/create:
 *     post:
 *      summary: Creates a new book
 *      tags: [Partnership]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Partnership'
 *      responses:
 *        "200":
 *          description: created Partnership.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Partnership' 
 * 
 */

/**
 * @swagger
 *  /partnerships/getAll:
 *    get:
 *      summary: List of all Partnerships
 *      tags: [Partnership]
 *      responses:
 *        "200":
 *          description: list of partnerships.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Partnership'
 * 
 */

/**
 * @swagger
 * /partnerships/delete/{partnershipId}:
 *    delete:
 *      summary: Delete partnership by id
 *      tags: [Partnership]
 *      parameters:
 *        - in: path
 *          name: partnershipId
 *          schema:
 *            type: string
 *          required: true
 *          description: Partnership id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Partnership not found.
 * 
 */

/**
 * @swagger
 * /partnerships/update/{partnershipId}:
 *   put:
 *      summary: Update partnership
 *      tags: [Partnership]
 *      parameters:
 *        - in: path
 *          name: partnershipId
 *          schema:
 *            type: string
 *          required: true
 *          description: Partnership id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Partnership'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Partnership not found.
 * 
 */