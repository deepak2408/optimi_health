const express = require('express');
const validate = require('../../middlewares/validate');
const tagsValidation = require('../../validations/tag.validation');
const tagsController = require('../../controllers/tag.controller');


const router = express.Router();

router.post('/create' ,validate(tagsValidation.createTags), tagsController.createTags );
router.get('/getAll' ,validate(tagsValidation.getTags), tagsController.getTags);
router.delete('/delete/:tagId' ,validate(tagsValidation.deleteTags), tagsController.deleteTags);
router.put('/update/:tagId' ,validate(tagsValidation.updateTags), tagsController.updateTags);

module.exports = router;