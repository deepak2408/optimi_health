const express = require('express');
const validate = require('../../middlewares/validate');
const progressionExerciseValidation = require('../../validations/ProgressionExercise.validation');
const progressionExerciseController = require('../../controllers/ProgressionExercise.controller');

const router = express.Router();

router.post('/create' ,validate(progressionExerciseValidation.createProgressionExercise), progressionExerciseController.createProgressionExercise );
router.get('/getAll' ,validate(progressionExerciseValidation.getProgressionExercise), progressionExerciseController.getProgressionExercise);
router.delete('/delete/:progressionExerciseId' ,validate(progressionExerciseValidation.deleteProgressionExercise), progressionExerciseController.deleteProgressionExercise);
router.put('/update/:progressionExerciseId' ,validate(progressionExerciseValidation.updateProgressionExercise), progressionExerciseController.updateProgressionExercise);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      ProgressionExercise:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          progressionExerciseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of progressionExercsie
 *        example:
 *           title: title of progressionExercise
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ProgressionExercise
 *   description: API to manage ProgressionExercise .
 */


/**
 * @swagger
 *  /progressionExercise/create:
 *     post:
 *      summary: Creates new progressionExercise
 *      tags: [ProgressionExercise]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ProgressionExercise'
 *      responses:
 *        "200":
 *          description: created ProgressionExercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgressionExercise' 
 * 
 */


/**
 * @swagger
 *  /progressionExercise/getAll:
 *    get:
 *      summary: List of all progressionExercise
 *      tags: [ProgressionExercise]
 *      responses:
 *        "200":
 *          description: list of ProgressionExercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgressionExercise'
 * 
 */


/**
 * @swagger
 *  /progressionExercise/{progressionExerciseId}:
 *    delete:
 *      summary: Delete progressionExercise by Id
 *      tags: [ProgressionExercise]
 *      parameters:
 *        - in: path
 *          name: progressionExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description:  progressionExercise objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: progressionExercise Id not found.
 */


/**
 * @swagger
 *  /progression/{progressionExerciseId}:
 *  put:
 *      summary: Update progressionExercise by Id
 *      tags: [ProgressionExercise]
 *      parameters:
 *        - in: path
 *          name: progressionExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: progressionExercise objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ProgressionExercise'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: progressionExercise not found.
 */

