const express = require('express');
const selfManageProgramController = require('../../controllers/SelfManageProgram.controller');
const selfManageProgramValidation = require('../../validations/SelfManageProgram.validation');
const validate = require('../../middlewares/validate');

const router = express.Router();

router.post(
  '/create',
  validate(selfManageProgramValidation.createSelfManageProgram),
  selfManageProgramController.createSelfManageProgram
);
router.get(
  '/getAll',
  validate(selfManageProgramValidation.getSelfManagePrograms),
  selfManageProgramController.getSelfManagePrograms
);
router.get(
  '/get/:selfManageProgramId',
  validate(selfManageProgramValidation.getSelfManageProgram),
  selfManageProgramController.getSelfManageProgram
);
router.delete(
  '/delete/:selfManageProgramId',
  validate(selfManageProgramValidation.deleteSelfManageProgram),
  selfManageProgramController.deleteSelfManageProgram
);
router.put(
  '/update/:selfManageProgramId',
  validate(selfManageProgramValidation.updateSelfManageProgram),
  selfManageProgramController.updateSelfManageProgram
);

module.exports = router;
