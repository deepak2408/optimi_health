const express = require('express');
const validate = require('../../middlewares/validate');
const { upload } = require('../../middlewares/upload');
const equipmentsValidation = require('../../validations/Equipments.validation');
const equipmentsController = require('../../controllers/Equipments.controller');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(equipmentsValidation.createEquipments),
  baseUrl,
  equipmentsController.createEquipments
);
router.get('/getAll' , validate(equipmentsValidation.getEquipments), equipmentsController.getEquipments);
router.delete('/delete/:equipmentsId' , validate(equipmentsValidation.deleteEquipments), equipmentsController.deleteEquipments);
router.put(
  '/update/:equipmentsId',
  upload.single('image'),
  validate(equipmentsValidation.updateEquipments),
  baseUrl,
  equipmentsController.updateEquipments
);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Equipments:
 *        type: object
 *        required:
 *          - title
 *          - image
 *        properties:
 *          equipmentId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of Equipment
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Program
 *           image: image  of program in jpg,png format
 *           
 */


/**
 * @swagger
 * tags:
 *   name: Equipment
 *   description: API to manage Equipmemt.
 */


/**
 * @swagger
 *  /equipments/create:
 *     post:
 *      summary: Creates new Equipment
 *      tags: [Equipment]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Equipments'
 *      responses:
 *        "200":
 *          description: created Program.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Equipments' 
 * 
 */


/**
 * @swagger
 *  /equipments/getAll:
 *    get:
 *      summary: List of all Equipment
 *      tags: [Equipment]
 *      responses:
 *        "200":
 *          description: list of Equipment.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Equipments'
 * 
 */


/**
 * @swagger
 *  /equipments/{equipmentsId}:
 *    delete:
 *      summary: Delete equipment by Id
 *      tags: [Equipment]
 *      parameters:
 *        - in: path
 *          name: equipmentId
 *          schema:
 *            type: string
 *          required: true
 *          description: Equipment objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Equipment Id not found.
 */


/**
 * @swagger
 *  /equipments/{equipmentId}:
 *  put:
 *      summary: Update Equipments
 *      tags: [Equipment]
 *      parameters:
 *        - in: path
 *          name: equipmentId
 *          schema:
 *            type: string
 *          required: true
 *          description: Equipment objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Equipments'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Equipments not found.
 */

