const express = require('express');
const validate = require('../../middlewares/validate');
const alternativeExerciseValidation = require('../../validations/AlternativeExercise.validation');
const alternativeExerciseController = require('../../controllers/AlternativeExercise.controller');

const router = express.Router();

router.post('/create' ,validate(alternativeExerciseValidation.createAlternativeExercise), alternativeExerciseController.createAlternativeExercise );
router.get('/getAll' ,validate(alternativeExerciseValidation.getAlternativeExercise), alternativeExerciseController.getAlternativeExercise);
router.delete('/delete/:alternativeExerciseId' ,validate(alternativeExerciseValidation.deleteAlternativeExercise), alternativeExerciseController.deleteAlternativeExercise);
router.put('/update/:alternativeExerciseId' ,validate(alternativeExerciseValidation.updateAlternativeExercise), alternativeExerciseController.updateAlternativeExercise);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      AlternativeExercise:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          alternativeExerciseId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of Alternative Exercise
 *        example:
 *           title: title of Alternative Exercise
 *           
 */


/**
 * @swagger
 * tags:
 *   name: AlternativeExercise
 *   description: API to manage Alternative Exercise.
 */


/**
 * @swagger
 *  /alternativeExercise/create:
 *     post:
 *      summary: Creates new Alternative Exercise
 *      tags: [AlternativeExercise]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AlternativeExercise'
 *      responses:
 *        "200":
 *          description: created BodyParts.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AlternativeExercise' 
 * 
 */


/**
 * @swagger
 *  /alternativeExercise/getAll:
 *    get:
 *      summary: List of all AlternativeExercise
 *      tags: [AlternativeExercise]
 *      responses:
 *        "200":
 *          description: list of AlternativeExercise.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AlternativeExercise'
 * 
 */


/**
 * @swagger
 *  /altenativeExercise/{alternativeId}:
 *    delete:
 *      summary: Delete AlternativeExercise by Id
 *      tags: [AlternativeExercise]
 *      parameters:
 *        - in: path
 *          name: alternativeExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: alternativeExercise objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: AlternativeExercise Id not found.
 */


/**
 * @swagger
 *  /alternativeExercise/{alternativeExerciseId}:
 *  put:
 *      summary: Update AlternativeExercise
 *      tags: [AlternativeExercise]
 *      parameters:
 *        - in: path
 *          name: alternativeExerciseId
 *          schema:
 *            type: string
 *          required: true
 *          description: AlternativeExercise objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AlternativeExercise'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: AlternativeExercise not found.
 */

