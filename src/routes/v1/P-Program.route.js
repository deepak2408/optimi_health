const express = require('express');
const programController = require('../../controllers/P-Program.controller');
const { upload } = require('../../middlewares/upload');
const programValidation = require('../../validations/P-Program.validation');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(programValidation.createProgram),
  baseUrl,
  programController.createProgram
);
router.get('/getAll', validate(programValidation.getPrograms), programController.getPrograms);
router.get('/get/:programId', validate(programValidation.getProgram), programController.getProgram);
router.delete('/delete/:programId', validate(programValidation.deleteProgram), programController.deleteProgram);
router.put(
  '/update/:programId',
  upload.single('image'),
  validate(programValidation.updateProgram),
  baseUrl,
  programController.updateProgram
);



module.exports = router;



/**
 * @swagger
 *  components:
 *    schemas:
 *      Program:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - programGoal 
 *          - programLevel
 *          - programPhases
 *          - status
 *          - image
 *        properties:
 *          programId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of Program
 *          description:
 *            type: string
 *            description: description of Program. 
 *          programGoal:
 *            type: string
 *            description: ObjectId of ProgramGoal.
 *          programLevel:
 *            type: string
 *            description: ObjectId of ProgramLevel.
 *          programPhases:
 *            type: array
 *            description: All the program phases.
 *          status:
 *            type: string
 *            description: status of progrma.
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Program
 *           description: description of Program
 *           programGoal: 623c7a4156f148136cfcdc69
 *           programLevel: 623c7a4156f148136cfcdc69
 *           programPhases: [623c7a4156f148136cfcdc69]
 *           status: stutus of program
 *           image: image  of program in jpg,png format
 *           
 */


/**
 * @swagger
 * tags:
 *   name: Program
 *   description: API to manage Program.
 */


/**
 * @swagger
 *  /program/create:
 *     post:
 *      summary: Creates new Program
 *      tags: [Program]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Program'
 *      responses:
 *        "200":
 *          description: created Program.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Program' 
 * 
 */


/**
 * @swagger
 *  /program/getAll:
 *    get:
 *      summary: List of all Program
 *      tags: [Program]
 *      responses:
 *        "200":
 *          description: list of Program.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Program'
 * 
 */

/**
 * @swagger
 *  /program/{programId}:
 *    get:
 *      summary: Get Program by Id
 *      tags: [Program]
 *      parameters:
 *        - in: path
 *          name: programId
 *          schema:
 *            type: string
 *          required: true
 *          description: Program objectId
 *      responses:
 *        "200":
 *          description: list of Program.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Program'
 *        "404":
 *          description: Program not found.
 */

/**
 * @swagger
 *  /program/{programId}:
 *  put:
 *      summary: Update Program
 *      tags: [Program]
 *      parameters:
 *        - in: path
 *          name: programId
 *          schema:
 *            type: string
 *          required: true
 *          description: Program objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Program'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Program not found.
 */

/**
 * @swagger
 *  /program/{programId}:
 *    delete:
 *      summary: Delete program by Id
 *      tags: [Program]
 *      parameters:
 *        - in: path
 *          name: programId
 *          schema:
 *            type: string
 *          required: true
 *          description: Program objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Program Id not found.
 */

     