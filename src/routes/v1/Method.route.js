const express = require('express');
const validate = require('../../middlewares/validate');
const  methodValidation = require('../../validations/Method.validation');
const methodController = require('../../controllers/Method.controller');


const router = express.Router();

router.post('/create' ,validate(methodValidation.createMethod), methodController.createMethod );
router.get('/getAll' ,validate(methodValidation.getMethod), methodController.getMethod);
router.delete('/delete/:methodId' ,validate(methodValidation.deleteMethod), methodController.deleteMethod);
router.put('/update/:methodId' ,validate(methodValidation.updateMethod), methodController.updateMethod);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Method:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          methodId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of method
 *        example:
 *           title: title of method
 *           
 */


/**
 * @swagger
 * tags:
 *   name: Method
 *   description: API to manage Method .
 */


/**
 * @swagger
 *  /method/create:
 *     post:
 *      summary: Creates new Method
 *      tags: [Method]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Method'
 *      responses:
 *        "200":
 *          description: created FocusPoints.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Method' 
 * 
 */


/**
 * @swagger
 *  /method/getAll:
 *    get:
 *      summary: List of all method
 *      tags: [Method]
 *      responses:
 *        "200":
 *          description: list of Method.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Method'
 * 
 */


/**
 * @swagger
 *  /method/{methodId}:
 *    delete:
 *      summary: Delete Method by Id
 *      tags: [Method]
 *      parameters:
 *        - in: path
 *          name: MethodId
 *          schema:
 *            type: string
 *          required: true
 *          description:  method objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: method Id not found.
 */


/**
 * @swagger
 *  /method/{methodId}:
 *  put:
 *      summary: Update method by Id
 *      tags: [Method]
 *      parameters:
 *        - in: path
 *          name: methodId
 *          schema:
 *            type: string
 *          required: true
 *          description: method objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Method'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: method not found.
 */

