const express = require('express');
const validate = require('../../middlewares/validate');
const { upload } = require('../../middlewares/upload');
const programGoalValidation = require('../../validations/P-Goal.validation');
const programGoalController = require('../../controllers/P-Goal.controller');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(programGoalValidation.createProgramGoal),
  baseUrl,
  programGoalController.createProgramGoal
);
router.get('/getAll' ,validate(programGoalValidation.getProgramGoal), programGoalController.getProgramGoal);
router.delete('/delete/:programGoalId' ,validate(programGoalValidation.deleteProgramGoal), programGoalController.deleteProgramGoal);
router.put(
  '/update/:programGoalId',
  upload.single('image'),
  validate(programGoalValidation.updateProgramGoal),
  baseUrl,
  programGoalController.updateProgramGoal
);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      ProgramGoal:
 *        type: object
 *        required:
 *          - title
 *          - image
 *        properties:
 *          programGoalId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of ProgramGoal
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Program
 *           image: image  of programGoal in jpg,png format
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ProgramGoal
 *   description: API to manage ProgramGoal.
 */


/**
 * @swagger
 *  /programGoal/create:
 *     post:
 *      summary: Creates new ProgramGoal
 *      tags: [ProgramGoal]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/ProgramGoal'
 *      responses:
 *        "200":
 *          description: created ProgramGoal.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgramGoal' 
 * 
 */


/**
 * @swagger
 *  /programGoal/getAll:
 *    get:
 *      summary: List of all ProgramGoal
 *      tags: [ProgramGoal]
 *      responses:
 *        "200":
 *          description: list of ProgramGoal.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ProgramGoal'
 * 
 */


/**
 * @swagger
 *  /programGoal/{programGoalId}:
 *    delete:
 *      summary: Delete programGoal by Id
 *      tags: [ProgramGoal]
 *      parameters:
 *        - in: path
 *          name: programGoalId
 *          schema:
 *            type: string
 *          required: true
 *          description: ProgramGoal objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ProgramGoal Id not found.
 */


/**
 * @swagger
 *  /programGoal/{programGoalId}:
 *  put:
 *      summary: Update ProgramGoal
 *      tags: [ProgramGoal]
 *      parameters:
 *        - in: path
 *          name: programGoalId
 *          schema:
 *            type: string
 *          required: true
 *          description: programGoal objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Equipments'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: ProgramGoal not found.
 */

