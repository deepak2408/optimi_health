const express = require('express');
const validate = require('../../middlewares/validate');
const  phaseProgressionValidation = require('../../validations/PhaseProgression.validation');
const phaseProgressionController = require('../../controllers/PhaseProgression.controller');


const router = express.Router();

router.post('/create' ,validate(phaseProgressionValidation.createPhaseProgression), phaseProgressionController.createPhaseProgression );
router.get('/getAll' ,validate(phaseProgressionValidation.getPhaseProgression), phaseProgressionController.getPhaseProgression);
router.delete('/delete/:phaseProgressionId' ,validate(phaseProgressionValidation.deletePhaseProgression), phaseProgressionController.deletePhaseProgression);
router.put('/update/:phaseProgressionId' ,validate(phaseProgressionValidation.updatePhaseProgression), phaseProgressionController.updatePhaseProgression);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      PhaseProgression:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          phaseProgressionId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of PhaseProgression
 *        example:
 *           title: title of PhaseProgression
 *           
 */


/**
 * @swagger
 * tags:
 *   name: PhaseProgression
 *   description: API to manage PhaseProgression .
 */


/**
 * @swagger
 *  /phaseProgression/create:
 *     post:
 *      summary: Creates new PhaseProgression
 *      tags: [PhaseProgression]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PhaseProgression'
 *      responses:
 *        "200":
 *          description: created PhaseProgression.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/PhaseProgression' 
 * 
 */


/**
 * @swagger
 *  /phaseProgression/getAll:
 *    get:
 *      summary: List of all phaseProgression
 *      tags: [PhaseProgression]
 *      responses:
 *        "200":
 *          description: list of PhaseProgression.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/PhaseProgression'
 * 
 */


/**
 * @swagger
 *  /phaseProgression/{phaseProgressionId}:
 *    delete:
 *      summary: Delete PhaseProgression by Id
 *      tags: [PhaseProgression]
 *      parameters:
 *        - in: path
 *          name: phaseProgressionId
 *          schema:
 *            type: string
 *          required: true
 *          description:  phaseProgression objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: phaseProgression Id not found.
 */


/**
 * @swagger
 *  /phaseProgression/{phaseProgressionId}:
 *  put:
 *      summary: Update phaseProgression by Id
 *      tags: [PhaseProgression]
 *      parameters:
 *        - in: path
 *          name: phaseProgressionId
 *          schema:
 *            type: string
 *          required: true
 *          description: phaseProgression objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PhaseProgression'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: PhaseProgression not found.
 */

