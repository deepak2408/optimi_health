const express = require('express');
const validate = require('../../middlewares/validate');
const professionalValidation = require('../../validations/Professional.validation');
const professionalController = require('../../controllers/Professional.controller');

const router = express.Router();

router.post('/create', validate(professionalValidation.createProfessional), professionalController.createProfessional);
router.get('/getAll', validate(professionalValidation.getProfessionals), professionalController.getProfessionals);
router.get('/get/:professionalId', validate(professionalValidation.getProfessional), professionalController.getProfessional);
router.delete(
  '/delete/:professionalId',
  validate(professionalValidation.deleteProfessional),
  professionalController.deleteProfessional
);
router.put(
  '/update/:professionalId',
  validate(professionalValidation.updateProfessional),
  professionalController.updateProfessional
);
router.put('/check/:professionalId', validate(professionalValidation.checkStatus), professionalController.checkStatus);
router.post('/login', validate(professionalValidation.login), professionalController.login);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      Professional:
 *        type: object
 *        required:
 *          - firstName
 *          - lastName
 *          - age 
 *          - country
 *          - state
 *          - city
 *          - address
 *          - zip
 *          - phoneNo
 *          - email
 *          - password
 *          - status
 *        properties:
 *          professionalId:
 *            type: string
 *          firstName:
 *            type: string
 *            description: firstName of professional 
 *          lastName:
 *            type: string
 *            description: lastName of professional. 
 *          age:
 *            type: number
 *            description: age of professional.
 *          country:
 *            type: string
 *            description: country of professional.
 *          state:
 *            type: string
 *            description: state of professional.
 *          city:
 *            type: string
 *            description: city of professional.
 *          address:
 *            type: string
 *            description: address of professional
 *          zip:
 *            type: number
 *            description: zipcode of professional
 *          phoneNo:
 *            type: string
 *            description: phoneNo of professional
 *          email:
 *            type: string
 *            description: email of professional
 *          password:
 *            type: string
 *            description: password of professional
 *          status:
 *             type: boolean
 *             description: status of professional
 *        example:
 *           firstName: firstName
 *           lastName: lastname
 *           age: 23
 *           country: India
 *           state: UttarPradesh
 *           city: kanpur
 *           address: address 
 *           zip: zip code of professional
 *           phoneNo: 2498534853
 *           email: hello@234.com
 *           password: pasw@12
 *           status: true 
 *           
 */


/**
 * @swagger
 * tags:
 *   name: Professional
 *   description: API to manage Professional
 */


/**
 * @swagger
 *  /professional/create:
 *     post:
 *      summary: Creates new Professional
 *      tags: [Professional]
 *      requestBody:
 *        required: true
 *        content:
 *          application/form-data:
 *            schema:
 *              $ref: '#/components/schemas/Professional'
 *      responses:
 *        "200":
 *          description: created Professional.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Professional'
 * 
 */


/**
 * @swagger
 *  /professional/getAll:
 *    get:
 *      summary: List of all Professional
 *      tags: [Professional]
 *      responses:
 *        "200":
 *          description: list of Professional.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Professional'
 * 
 */

/**
 * @swagger
 *  /professional/{professionalId}:
 *    get:
 *      summary: Get Professional by Id
 *      tags: [Professional]
 *      parameters:
 *        - in: path
 *          name: professionalId
 *          schema:
 *            type: string
 *          required: true
 *          description: Professional objectId
 *      responses:
 *        "200":
 *          description: list of Professional.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Professional'
 *        "404":
 *          description: professional not found.
 */

/**
 * @swagger
 *  /professional/{professionalId}:
 *    delete:
 *      summary: Delete professional by Id
 *      tags: [Professional]
 *      parameters:
 *        - in: path
 *          name: professionalId
 *          schema:
 *            type: string
 *          required: true
 *          description: Professional objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Professional Id not found.
 */


/**
 * @swagger
 *  /professional/{professionalId}:
 *  put:
 *      summary: Update Program
 *      tags: [Professional]
 *      parameters:
 *        - in: path
 *          name: professionalId
 *          schema:
 *            type: string
 *          required: true
 *          description: professional objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Professional'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Professional not found.
 */
  

/**
 * @swagger
 *  /professional/check/{professionalId}:
 *  put:
 *      summary: Update status
 *      tags: [Professional]
 *      parameters:
 *        - in: path
 *          name: professionalId
 *          schema:
 *            type: string
 *          required: true
 *          description: professional objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *             schema:
 *              type: object
 *              properties:
 *               status:
 *                 type: boolean
 *             example:
 *               status: false 
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Professional not found.
 */


