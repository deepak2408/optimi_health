const express = require('express');
const validate = require('../../middlewares/validate');
const  forumTagsValidation = require('../../validations/ForumTags.validation');
const forumTagsController = require('../../controllers/ForumTags.controller');


const router = express.Router();

router.post('/create' ,validate(forumTagsValidation.createForumTags), forumTagsController.createForumTags );
router.get('/getAll' ,validate(forumTagsValidation.getForumTags), forumTagsController.getForumTags);
router.delete('/delete/:forumTagId' ,validate(forumTagsValidation.deleteForumTags), forumTagsController.deleteForumTags);
router.put('/update/:forumTagId' ,validate(forumTagsValidation.updateForumTags), forumTagsController.updateForumTags);

module.exports = router;

/**
 * @swagger
 *  components:
 *    schemas:
 *      ForumTags:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          forumTagId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of forumTag
 *        example:
 *           title: title of forumTag
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ForumTags
 *   description: API to manage ForumTags .
 */


/**
 * @swagger
 *  /forumTags/create:
 *     post:
 *      summary: Creates new ForumTags
 *      tags: [ForumTags]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ForumTags'
 *      responses:
 *        "200":
 *          description: created ForumTag.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ForumTags' 
 * 
 */


/**
 * @swagger
 *  /forumTags/getAll:
 *    get:
 *      summary: List of all forumTag
 *      tags: [ForumTags]
 *      responses:
 *        "200":
 *          description: list of FocusTags.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ForumTags'
 * 
 */


/**
 * @swagger
 *  /forumTags/{forumTagId}:
 *    delete:
 *      summary: Delete ForumTags by Id
 *      tags: [ForumTags]
 *      parameters:
 *        - in: path
 *          name: forumTagsId
 *          schema:
 *            type: string
 *          required: true
 *          description:  forumTag objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ForumTag Id not found.
 */


/**
 * @swagger
 *  /forumTags/{forumTagId}:
 *  put:
 *      summary: Update ForumTags by Id
 *      tags: [ForumTags]
 *      parameters:
 *        - in: path
 *          name: forumTagId
 *          schema:
 *            type: string
 *          required: true
 *          description: forumTags objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ForumTags'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: forumTag not found.
 */

