const express = require('express');
const validate = require('../../middlewares/validate');
const exerciseTypeValidation = require('../../validations/ExerciseType.validation');
const exerciseTypeController = require('../../controllers/ExerciseType.controller');

const router = express.Router();

router.post('/create' ,validate(exerciseTypeValidation.createExerciseType), exerciseTypeController.createExerciseType );
router.get('/getAll' ,validate(exerciseTypeValidation.getExerciseType), exerciseTypeController.getExerciseType);
router.delete('/delete/:exerciseTypeId' ,validate(exerciseTypeValidation.deleteExerciseType), exerciseTypeController.deleteExerciseType);
router.put('/update/:exerciseTypeId' ,validate(exerciseTypeValidation.updateExerciseType), exerciseTypeController.updateExerciseType);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      ExerciseType:
 *        type: object
 *        required:
 *          - title
 *        properties:
 *          exerciseTypeId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of type of Exercise
 *        example:
 *           title: title of ExerciseType Exercise
 *           
 */


/**
 * @swagger
 * tags:
 *   name: ExerciseType
 *   description: API to manage ExerciseType .
 */


/**
 * @swagger
 *  /exerciseType/create:
 *     post:
 *      summary: Creates new ExerciseType
 *      tags: [ExerciseType]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExerciseType'
 *      responses:
 *        "200":
 *          description: created ExerciseType.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExerciseType' 
 * 
 */


/**
 * @swagger
 *  /exerciseType/getAll:
 *    get:
 *      summary: List of all ExclusionCriteria
 *      tags: [ExerciseType]
 *      responses:
 *        "200":
 *          description: list of ExerciseType.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExerciseType'
 * 
 */


/**
 * @swagger
 *  /exerciseType/{exerciseId}:
 *    delete:
 *      summary: Delete ExerciseType by Id
 *      tags: [ExerciseType]
 *      parameters:
 *        - in: path
 *          name: exerciseTypeId
 *          schema:
 *            type: string
 *          required: true
 *          description: exerciseType objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ExerciseType Id not found.
 */


/**
 * @swagger
 *  /exerciseType/{exerciseTypeId}:
 *  put:
 *      summary: Update ExerciseType by Id
 *      tags: [ExerciseType]
 *      parameters:
 *        - in: path
 *          name: exerciseTypeId
 *          schema:
 *            type: string
 *          required: true
 *          description: ExerciseType objectId
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExerciseType'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: ExerciseType not found.
 */

