const express = require('express');
const { forumQuestionController } = require('../../controllers');
const { upload } = require('../../middlewares/upload');
const { forumQuestionValidation } = require('../../validations');
const validate = require('../../middlewares/validate');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(forumQuestionValidation.createForumQuestion),
  baseUrl,
  forumQuestionController.createForumQuestion
);
router.get('/getAll', validate(forumQuestionValidation.getForumQuestions), forumQuestionController.getForumQuestions);
router.get(
  '/get/:forumQuestionId',
  validate(forumQuestionValidation.getForumQuestion),
  forumQuestionController.getForumQuestion
);
router.delete(
  '/delete/:forumQuestionId',
  validate(forumQuestionValidation.deleteForumQuestion),
  forumQuestionController.deleteForumQuestion
);
router.put(
  '/update/:forumQuestionId',
  upload.single('image'),
  validate(forumQuestionValidation.updateForumQuestion),
  baseUrl,
  forumQuestionController.updateForumQuestion
);

module.exports = router;

/**
 * @swagger
 *  components:
 *    schemas:
 *      ForumQuestion:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - tag
 *          - featured
 *          - status
 *          - image
 *        properties:
 *          forumQuestionId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of ForumQuestion
 *          description:
 *            type: string
 *            description: description of ForumQuestion.
 *          tag:
 *            type: string
 *            description: ObjectId of tag.
 *          featured:
 *            type: string
 *            description: featured of ForumQuestion.
 *          status:
 *            type: string
 *            description: status of ForumQuestion.
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of Program
 *           description: description of Program
 *           tag: 623c7a4156f148136cfcdc69
 *           featured: no
 *           status: stutus of forumQuestion
 *           image: image in jpg,png format
 *
 */

/**
 * @swagger
 * tags:
 *   name: ForumQuestion
 *   description: API to manage ForumQuestion.
 */

/**
 * @swagger
 *  /forumQuestion/create:
 *     post:
 *      summary: Creates new ForumQuestion
 *      tags: [ForumQuestion]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/ForumQuestion'
 *      responses:
 *        "200":
 *          description: created ForumQuestion.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ForumQuestion'
 *
 */

/**
 * @swagger
 *  /forumQuestion/getAll:
 *    get:
 *      summary: List of all ForumQuestion
 *      tags: [ForumQuestion]
 *      responses:
 *        "200":
 *          description: list of ForumQuestion.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ForumQuestion'
 *
 */

/**
 * @swagger
 *  /forumQuestion/{forumQuestionId}:
 *    get:
 *      summary: Get ForumQuestion by Id
 *      tags: [ForumQuestion]
 *      parameters:
 *        - in: path
 *          name: forumQuestionId
 *          schema:
 *            type: string
 *          required: true
 *          description: ForumQuestion objectId
 *      responses:
 *        "200":
 *          description: list of ForumQuestion.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ForumQuestion'
 *        "404":
 *          description: ForumQuestion not found.
 */

/**
 * @swagger
 *  /forumQuestion/{ForumQuestionId}:
 *  put:
 *      summary: Update forumQuestion
 *      tags: [ForumQuestion]
 *      parameters:
 *        - in: path
 *          name: forumQuestionId
 *          schema:
 *            type: string
 *          required: true
 *          description: forumQuestion objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/ForumQuestion'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: ForumQuestion not found.
 */

/**
 * @swagger
 *  /forumQuestion/{forumQuestionId}:
 *    delete:
 *      summary: Delete forumQuestion by Id
 *      tags: [ForumQuestion]
 *      parameters:
 *        - in: path
 *          name: forumQuestionId
 *          schema:
 *            type: string
 *          required: true
 *          description: ForumQuestion objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: ForumQuestion Id not found.
 */
