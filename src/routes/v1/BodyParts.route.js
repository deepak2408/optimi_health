const express = require('express');
const validate = require('../../middlewares/validate');
const { upload } = require('../../middlewares/upload');
const bodyPartsValidation = require('../../validations/BodyParts.validation');
const bodyPartsController = require('../../controllers/BodyParts.controller');
const { baseUrl } = require('../../middlewares/getBaseUrl');

const router = express.Router();

router.post(
  '/create',
  upload.single('image'),
  validate(bodyPartsValidation.createBodyParts),
  baseUrl,
  bodyPartsController.createBodyParts
);
router.get('/getAll', validate(bodyPartsValidation.getBodyParts), bodyPartsController.getBodyParts);
router.delete('/delete/:bodyPartsId', validate(bodyPartsValidation.deleteBodyParts), bodyPartsController.deleteBodyParts);
router.put(
  '/update/:bodyPartsId',
  upload.single('image'),
  validate(bodyPartsValidation.updateBodyParts),
  baseUrl,
  bodyPartsController.updateBodyParts
);

module.exports = router;


/**
 * @swagger
 *  components:
 *    schemas:
 *      BodyParts:
 *        type: object
 *        required:
 *          - title
 *          - image
 *        properties:
 *          bodyPartsId:
 *            type: string
 *          title:
 *            type: string
 *            description: title of BodyParts
 *          image:
 *            type: string
 *            description: image in jpg,png format
 *        example:
 *           title: title of BodyParts
 *           image: image  of program in jpg,png format
 *           
 */


/**
 * @swagger
 * tags:
 *   name: BodyParts
 *   description: API to manage BodyParts.
 */


/**
 * @swagger
 *  /bodyParts/create:
 *     post:
 *      summary: Creates new BodyParts
 *      tags: [BodyParts]
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/BodyParts'
 *      responses:
 *        "200":
 *          description: created BodyParts.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/BodyParts' 
 * 
 */


/**
 * @swagger
 *  /bodyParts/getAll:
 *    get:
 *      summary: List of all BodyParts
 *      tags: [BodyParts]
 *      responses:
 *        "200":
 *          description: list of BodyParts.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/BodyParts'
 * 
 */


/**
 * @swagger
 *  /bodyParts/{bodyPartsId}:
 *    delete:
 *      summary: Delete BodyParts by Id
 *      tags: [BodyParts]
 *      parameters:
 *        - in: path
 *          name: bodyPartsId
 *          schema:
 *            type: string
 *          required: true
 *          description: BodyParts objectId
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: BodyParts Id not found.
 */


/**
 * @swagger
 *  /bodyParts/{bodyPartsId}:
 *  put:
 *      summary: Update Bodyparts
 *      tags: [BodyParts]
 *      parameters:
 *        - in: path
 *          name: bodyPartsId
 *          schema:
 *            type: string
 *          required: true
 *          description: BodyParts objectId
 *      requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              $ref: '#/components/schemas/BodyParts'
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: bodyParts not found.
 */




