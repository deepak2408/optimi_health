const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createFocusPoints = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteFocusPoints = {
    params : Joi.object().keys({
        focusPointsId : Joi.string().custom(objectId),
    })
}

const updateFocusPoints = {
    params : Joi.object().keys({
        focusPointsId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getFocusPoints = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createFocusPoints,
    deleteFocusPoints,
    updateFocusPoints,
    getFocusPoints,
}