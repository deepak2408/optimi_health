const Joi = require('joi-oid');

const createPhases = {
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        phaseProgression : Joi.objectId(),
        phaseGoal : Joi.objectId(),
        sessionInARow :  Joi.number().required(),
        phaseTest : Joi.objectId(),
        phaseSessions : Joi.array().items(Joi.object().keys({
            _id : Joi.objectId(),
          })),
        status : Joi.string().required(),
    }),
}

const deletePhases = {
    params : Joi.object().keys({
        phaseId : Joi.objectId(),
    })
}

const getPhases = {
    query: Joi.object().keys({
      title: Joi.string(),
      description : Joi.string(),
      role: Joi.string(),
      sortBy: Joi.string(),
      limit: Joi.number().integer(),
      page: Joi.number().integer(),
    }),
  };

  const getPhase = {
      params : Joi.object().keys({
          phaseId : Joi.objectId(),
      })
  }

  const updatePhases = {
    params : Joi.object().keys({
        phaseId : Joi.objectId(),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        phaseProgression : Joi.objectId(),
        phaseGoal : Joi.objectId(),
        sessionInARow :  Joi.number().required(),
        phaseTest : Joi.objectId(),
        phaseSessions : Joi.array().items(Joi.object().keys({
            _id : Joi.objectId(),
          })),
        status : Joi.string().required(),
    }),
}

module.exports = { 
    createPhases,
    deletePhases,
    getPhases,
    updatePhases,
    getPhase
}