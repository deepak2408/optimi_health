const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createEquipments = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteEquipments = {
    params : Joi.object().keys({
        equipmentsId : Joi.string().custom(objectId),
    })
}

const updateEquipments = {
    params : Joi.object().keys({
        equipmentsId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getEquipments = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createEquipments,
    deleteEquipments,
    updateEquipments,
    getEquipments,
}