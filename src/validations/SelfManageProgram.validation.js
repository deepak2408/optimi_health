const Joi = require('joi-oid');

const createSelfManageProgram = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    stageCount: Joi.number().required(),
    stages: Joi.array().items(
      Joi.object().keys({
        currentStage: Joi.number(),
        exercises: Joi.array().items().required(),
        markers: Joi.array().items().required(),
        stageLevel: Joi.objectId(),
      })
    ),
  }),
};

const deleteSelfManageProgram = {
  params: Joi.object().keys({
    selfManageProgramId: Joi.objectId(),
  }),
};

const updateSelfManageProgram = {
  params: Joi.object().keys({
    selfManageProgramId: Joi.objectId(),
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
    stageCount: Joi.number().required(),
    stages: Joi.array().items(
      Joi.object().keys({
        currentStage: Joi.number(),
        exercises: Joi.array().items().required(),
        markers: Joi.array().items().required(),
        stageLevel: Joi.objectId(),
      })
    ),
  }),
};

const getSelfManagePrograms = {
  query: Joi.object().keys({
    name: Joi.string(),
    role: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getSelfManageProgram = {
  params: Joi.object().keys({
    selfManageProgramId: Joi.objectId(),
  }),
};

module.exports = {
  createSelfManageProgram,
  deleteSelfManageProgram,
  updateSelfManageProgram,
  getSelfManagePrograms,
  getSelfManageProgram,
};
