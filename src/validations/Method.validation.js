const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createMethod = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteMethod = {
    params : Joi.object().keys({
        methodId : Joi.string().custom(objectId),
    })
}

const updateMethod = {
    params : Joi.object().keys({
        methodId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getMethod = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createMethod,
    deleteMethod,
    updateMethod,
    getMethod
}