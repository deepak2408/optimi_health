const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createForumTags = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteForumTags = {
    params : Joi.object().keys({
        forumTagId : Joi.string().custom(objectId),
    })
}

const updateForumTags = {
    params : Joi.object().keys({
        forumTagId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getForumTags = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createForumTags,
    deleteForumTags,
    updateForumTags,
    getForumTags
}