const Joi = require('joi-oid');

const createExercise = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    bodyParts: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    focusPoints: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    equipments: Joi.objectId(),
    regressionExercise: Joi.objectId(),
    progressionExercise: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    alternativeExercise: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    exclusionsCriteria: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    level: Joi.objectId(),
    exerciseType: Joi.objectId(),
    videoUrl: Joi.string().required(),
    instruction: Joi.string().required(),
    tips: Joi.string(),
  }),
};

const deleteExercise = {
    params : Joi.object().keys({
        exerciseId : Joi.objectId(),
    })
}

const updateExercise = {
  params: Joi.object().keys({
    exerciseId: Joi.objectId(),
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    bodyParts: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    focusPoints: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    equipments: Joi.objectId(),
    regressionExercise: Joi.objectId(),
    progressionExercise: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    alternativeExercise: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    exclusionsCriteria: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    Level: Joi.objectId(),
    exerciseType: Joi.objectId(),
    videoUrl: Joi.string().required(),
    instruction: Joi.string().required(),
    tips: Joi.string(),
  }),
};

const getExercises = {
    query: Joi.object().keys({
        title: Joi.string(),
        role: Joi.string(),
        sortBy: Joi.string(),
        limit: Joi.number().integer(),
        page: Joi.number().integer(),
      }),
}

const getExercise = {
    params: Joi.object().keys({
      exerciseId: Joi.objectId(),
    }),
  };

module.exports = {
    createExercise,
    deleteExercise,
    updateExercise,
    getExercises,
    getExercise
}