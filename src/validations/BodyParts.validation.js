const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createBodyParts = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteBodyParts = {
    params : Joi.object().keys({
        bodyPartsId : Joi.string().custom(objectId),
    })
}

const updateBodyParts = {
    params : Joi.object().keys({
        bodyPartsId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getBodyParts = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createBodyParts,
    deleteBodyParts,
    updateBodyParts,
    getBodyParts,
}