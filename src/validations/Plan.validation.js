const Joi = require('joi-oid');

const createPlan = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    shortDescription: Joi.string().required(),
    planPrice: Joi.number(),
    planVideo: Joi.string(),
    description: Joi.string(),
    planProgram: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    status: Joi.string().required(),
  }),
};

const deletePlan = {
  params: Joi.object().keys({
    planId: Joi.objectId(),
  }),
};

const updatePlan = {
  params: Joi.object().keys({
    planId: Joi.objectId(),
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    shortDescription: Joi.string().required(),
    planPrice: Joi.number(),
    planVideo: Joi.string(),
    description: Joi.string(),
    planProgram: Joi.array().items(
      Joi.object().keys({
        _id: Joi.objectId(),
      })
    ),
    status: Joi.string().required(),
  }),
};

const getPlans = {
  query: Joi.object().keys({
    title: Joi.string(),
    role: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getPlan = {
  params: Joi.object().keys({
    planId: Joi.objectId(),
  }),
};

module.exports = {
  createPlan,
  deletePlan,
  updatePlan,
  getPlans,
  getPlan,
};
