const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createRegressionExercise = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteRegressionExercise = {
    params : Joi.object().keys({
        regressionExerciseId : Joi.string().custom(objectId),
    })
}

const updateRegressionExercise = {
    params : Joi.object().keys({
        regressionExerciseId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getRegressionExercise = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createRegressionExercise,
    deleteRegressionExercise,
    updateRegressionExercise,
    getRegressionExercise,
}