const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProgramPhases = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteProgramPhases = {
    params : Joi.object().keys({
        programPhaseId : Joi.string().custom(objectId),
    })
}

const updateProgramPhases = {
    params : Joi.object().keys({
        programPhaseId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getProgramPhases = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createProgramPhases,
    deleteProgramPhases,
    updateProgramPhases,
    getProgramPhases,
}