const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createPhaseProgression = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deletePhaseProgression = {
    params : Joi.object().keys({
        phaseProgressionId : Joi.string().custom(objectId),
    })
}

const updatePhaseProgression = {
    params : Joi.object().keys({
        phaseProgressionId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getPhaseProgression = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createPhaseProgression,
    deletePhaseProgression,
    updatePhaseProgression,
    getPhaseProgression
}