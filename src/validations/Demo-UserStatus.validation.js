const Joi = require('joi');

const createDemoUser = {
  body: Joi.object().keys({
    uniqueId: Joi.string().required(),
    status: Joi.string().valid('download', 'registered', 'triage', 'plan'),
  }),
};

module.exports = { createDemoUser };
