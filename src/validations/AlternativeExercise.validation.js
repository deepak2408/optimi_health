const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createAlternativeExercise = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteAlternativeExercise = {
    params : Joi.object().keys({
        alternativeExerciseId : Joi.string().custom(objectId),
    })
}

const updateAlternativeExercise = {
    params : Joi.object().keys({
        alternativeExerciseId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getAlternativeExercise = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createAlternativeExercise,
    deleteAlternativeExercise,
    updateAlternativeExercise,
    getAlternativeExercise,
}