const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProgramLevel = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteProgramLevel = {
    params : Joi.object().keys({
        programLevelId : Joi.string().custom(objectId),
    })
}

const updateProgramLevel = {
    params : Joi.object().keys({
        programLevelId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getProgramLevel = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createProgramLevel,
    deleteProgramLevel,
    updateProgramLevel,
    getProgramLevel,
}