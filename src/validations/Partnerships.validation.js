const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createPartnerships = {
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        websiteUrl :  Joi.string().required(),
        affiliationLink :  Joi.string().required(),
        discount : Joi.number().required(),
        logo : Joi.string().required(),
    })
}

const deletePartnerships = {
    params : Joi.object().keys({
        partnershipId : Joi.string().custom(objectId),
    })
}

const updatePartnerships = {
    params : Joi.object().keys({
        partnershipId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        websiteUrl :  Joi.string().required(),
        affiliationLink :  Joi.string().required(),
        discount : Joi.number().required(),
        logo : Joi.string().required(),
    })
}

const getPartnerships = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createPartnerships,
    deletePartnerships,
    updatePartnerships,
    getPartnerships
}