const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createExerciseType = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteExerciseType = {
    params : Joi.object().keys({
        exerciseTypeId : Joi.string().custom(objectId),
    })
}

const updateExerciseType = {
    params : Joi.object().keys({
        exerciseTypeId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getExerciseType = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createExerciseType,
    deleteExerciseType,
    updateExerciseType,
    getExerciseType,
}