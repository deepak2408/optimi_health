const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProgramGoal = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteProgramGoal = {
    params : Joi.object().keys({
        programGoalId : Joi.string().custom(objectId),
    })
}

const updateProgramGoal = {
    params : Joi.object().keys({
        programGoalId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getProgramGoal = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createProgramGoal,
    deleteProgramGoal,
    updateProgramGoal,
    getProgramGoal,
}