const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createExclusionCriteria = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteExclusionCriteria = {
    params : Joi.object().keys({
        exclusionCriteriaId : Joi.string().custom(objectId),
    })
}

const updateExclusionCriteria = {
    params : Joi.object().keys({
        exclusionCriteriaId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getExclusionCriteria = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createExclusionCriteria,
    deleteExclusionCriteria,
    updateExclusionCriteria,
    getExclusionCriteria,
}