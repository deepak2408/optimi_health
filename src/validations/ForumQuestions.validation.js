const Joi = require('joi-oid');

const createForumQuestion = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    tag: Joi.objectId(),
    featured: Joi.string().required(),
    status: Joi.string().required(),
  }),
};

const deleteForumQuestion = {
  params: Joi.object().keys({
    forumQuestionId: Joi.objectId(),
  }),
};

const updateForumQuestion = {
  params: Joi.object().keys({
    forumQuestionId: Joi.objectId(),
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    tag: Joi.objectId(),
    featured: Joi.string().required(),
    status: Joi.string().required(),
  }),
};

const getForumQuestions = {
  query: Joi.object().keys({
    title: Joi.string(),
    role: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getForumQuestion = {
  params: Joi.object().keys({
    forumQuestionId: Joi.objectId(),
  }),
};

module.exports = {
  createForumQuestion,
  deleteForumQuestion,
  updateForumQuestion,
  getForumQuestions,
  getForumQuestion,
};
