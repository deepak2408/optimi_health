const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createPhaseTest = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deletePhaseTest = {
    params : Joi.object().keys({
        phaseTestId : Joi.string().custom(objectId),
    })
}

const updatePhaseTest = {
    params : Joi.object().keys({
        phaseTestId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getPhaseTest = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createPhaseTest,
    deletePhaseTest,
    updatePhaseTest,
    getPhaseTest
}