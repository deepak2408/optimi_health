const Joi = require('joi-oid');

const createProgram = {
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        programGoal : Joi.objectId(),
        programLevel : Joi.objectId(),
        programPhases : Joi.array().items(Joi.object().keys({
            _id : Joi.objectId(),
          })),
        status : Joi.string().required(),
    })
}

const deleteProgram = {
    params : Joi.object().keys({
        programId : Joi.objectId(),
    })
}

const updateProgram = {
    params : Joi.object().keys({
        programId : Joi.objectId(),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        programGoal : Joi.objectId(),
        programLevel : Joi.objectId(),
        programPhases : Joi.array().items(Joi.object().keys({
            _id : Joi.objectId(),
          })),
        status : Joi.string().required(),
    })
  
}

const getPrograms = {
    query: Joi.object().keys({
        title: Joi.string(),
        role: Joi.string(),
        sortBy: Joi.string(),
        limit: Joi.number().integer(),
        page: Joi.number().integer(),
      }),
}

const getProgram = {
    params: Joi.object().keys({
      programId: Joi.objectId(),
    }),
  };

module.exports = {
    createProgram,
    deleteProgram,
    updateProgram,
    getPrograms,
    getProgram
}