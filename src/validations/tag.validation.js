const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createTags = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteTags = {
    params : Joi.object().keys({
        tagId : Joi.string().custom(objectId),
    })
}

const updateTags = {
    params : Joi.object().keys({
        tagId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getTags = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createTags,
    deleteTags,
    updateTags,
    getTags
}