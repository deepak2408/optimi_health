const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProgressionExercise = {
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const deleteProgressionExercise = {
    params : Joi.object().keys({
        progressionExerciseId : Joi.string().custom(objectId),
    })
}

const updateProgressionExercise = {
    params : Joi.object().keys({
        progressionExerciseId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
    })
}

const getProgressionExercise = {
    query: Joi.object().keys({
        title: Joi.string(),
      }),
}

module.exports = {
    createProgressionExercise,
    deleteProgressionExercise,
    updateProgressionExercise,
    getProgressionExercise,
}