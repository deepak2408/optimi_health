const Joi = require('joi-oid');

const createPosts = {
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        tag : Joi.objectId(),
        featured : Joi.boolean().required(),
        status : Joi.string().required(),
    }),
}

const deletePosts = {
    params : Joi.object().keys({
        postId : Joi.objectId(),
    })
}

const getPosts = {
    query: Joi.object().keys({
      title: Joi.string(),
      description : Joi.string(),
      role: Joi.string(),
      sortBy: Joi.string(),
      limit: Joi.number().integer(),
      page: Joi.number().integer(),
    }),
  };

  const getPost = {
      params : Joi.object().keys({
          postId : Joi.objectId(),
      })
  }

  const updatePosts = {
    params : Joi.object().keys({
        postId : Joi.objectId(),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        tag : Joi.objectId(),
        featured : Joi.boolean().required(),
        status : Joi.string().required(),
    }),
}

module.exports = { 
    createPosts,
    deletePosts,
    getPosts,
    updatePosts,
    getPost
}