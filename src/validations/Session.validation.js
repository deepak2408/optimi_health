const Joi = require('joi-oid');
const { objectId } = require('./custom.validation');

const createSession = {
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        sessionDuration : Joi.number(),
        restBetweenExercise : Joi.number(),
        sessionExercises :  Joi.array().items(Joi.object().keys({
            exercise : Joi.objectId(),
            sets : Joi.number(),
            reps : Joi.number(),
            rest : Joi.number(),
            method : Joi.objectId(),
            bodyParts : Joi.objectId(),
            equipmentValue : Joi.number(),
          })),
    })
}

const deleteSession = {
    params : Joi.object().keys({
        sessionId : Joi.string().custom(objectId),
    })
}

const updateSession = {
    params : Joi.object().keys({
        sessionId : Joi.string().custom(objectId),
    }),
    body : Joi.object().keys({
        title : Joi.string().required(),
        description : Joi.string().required(),
        sessionDuration : Joi.number(),
        restBetweenExercise : Joi.number(),
        sessionExercises :  Joi.array().items(Joi.object().keys({
            exercise : Joi.objectId(),
            sets : Joi.number(),
            reps : Joi.number(),
            rest : Joi.number(),
            method : Joi.objectId(),
            bodyParts : Joi.objectId(),
            equipmentValue : Joi.number(),
          })),
    })
}

const getSessions = {
    query: Joi.object().keys({
        title: Joi.string(),
        role: Joi.string(),
        sortBy: Joi.string(),
        limit: Joi.number().integer(),
        page: Joi.number().integer(),
      }),
}

const getSession = {
    param: Joi.object().keys({
        sessionId : Joi.objectId(),
    })
}

module.exports = {
    createSession,
    deleteSession,
    updateSession,
    getSession,
    getSessions,
}