const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProfessional = {
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    age: Joi.number(),
    country: Joi.string(),
    state: Joi.string(),
    city: Joi.string(),
    address: Joi.string(),
    zip: Joi.number(),
    phoneNo: Joi.string().regex(/^[+]?(1\-|1\s|1|\d{3}\-|\d{3}\s|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }),
};

const deleteProfessional = {
  params: Joi.object().keys({
    professionalId: Joi.string().custom(objectId),
  }),
};

const updateProfessional = {
  params: Joi.object().keys({
    professionalId: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    age: Joi.number(),
    country: Joi.string(),
    state: Joi.string(),
    city: Joi.string(),
    address: Joi.string(),
    zip: Joi.number(),
    phoneNo: Joi.string().regex(/^[+]?(1\-|1\s|1|\d{3}\-|\d{3}\s|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }),
};

const getProfessionals = {
  query: Joi.object().keys({
    phoneNo: Joi.string(),
    email: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getProfessional = {
  params: Joi.object().keys({
    professionalId: Joi.string().custom(objectId),
  }),
};

const checkStatus = {
  params: Joi.object().keys({
    professionalId: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    status: Joi.boolean(),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }),
};

module.exports = {
  createProfessional,
  updateProfessional,
  getProfessionals,
  getProfessional,
  deleteProfessional,
  checkStatus,
  login,
};
