const response = require('../utils/response');

const catchAsync = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch((err) => {
    console.log('err', err);
    next(response.badRequest(err));
  });
};

module.exports = catchAsync;
