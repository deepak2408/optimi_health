const createError = require('http-errors');
const httpStatus = require('http-status');

const buildStatus = (error) => {
  if (typeof error === 'undefined') {
    return {
      success: true,
      errorCode: 0,
      errorMessage: '',
      error: null,
    };
    // eslint-disable-next-line no-else-return
  } else {
    const errorDetails =
      process.env.NODE_ENV === 'dev'
        ? {
            name: error.name,
            message: error.message,
            stacktrace: error.stack,
          }
        : {};

    return {
      success: false,
      errorCode: error.status,
      errorMessage: error.message,
      error: errorDetails,
    };
  }
};

const success = (res, status, body) => {
  res.status(status).json({
    responseStatus: buildStatus(),
    body,
  });
};

const requestFailed = (message) => createError(400, message);

const badRequest = (message) => createError(httpStatus.BAD_REQUEST, message);
const notFound = (message) => createError(404, message);

const validationError = (message) => createError(422, message);

const renderError = (res, error) => {
  if (!createError.isHttpError(error)) {
    // add this line to include winston logging
    winston.error(error);
  }

  // eslint-disable-next-line no-param-reassign
  if (!error.status) error.status = 500;

  // render the error json
  res.status(error.status).json({
    responseStatus: buildStatus(error),
    body: null,
  });
};

module.exports = {
  notFound,
  requestFailed,
  validationError,
  renderError,
  success,
  badRequest,
};
