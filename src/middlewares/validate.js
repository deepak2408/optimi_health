const Joi = require('joi');
const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');

const validate = (schema) => (req, res, next) => {
  // console.log(req);
  const validSchema = pick(schema, ['params', 'query', 'body']);
  // console.log(validSchema);
  const object = pick(req, Object.keys(validSchema));
  //console.log(object);
  const { value, error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' }, abortEarly: false })
    .validate(object);
  //console.log(value);

  if (error) {
    console.log(error);
    const errorMessage = error.details.map((details) => details.message).join(', ');
    return next(new ApiError(httpStatus.BAD_REQUEST, errorMessage));
  }
  Object.assign(req, value);
  // console.log(req);
  return next();
};;;

module.exports = validate;
