const url = require('url');

const baseUrl = (req, res, next) => {
  const myUrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
  });
  req.baseUrl = myUrl;
  next();
};

module.exports = {
  baseUrl,
};
