const mongoose = require('mongoose');
const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');
//const express = require('express');
//const app = express();
// const server = require('http').createServer(app);
// const io = require('socket.io')(server);


 let server;
  mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');
  
  //  io.on('connection', (socket) => {
  //    console.log('socket',socket);
  //    console.log('socket is active ');

  //    socket.on('event',(payload)=> {
  //      console.log('payload',payload);
  //      io.emit('hello',payload.id);
  //    });
  // });

  // server.listen(3000, () => {console.log('server is listening...')});
  
  server = app.listen(config.port, () => {
    logger.info(`Listening to port ${config.port}`);
  });
});



const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
  
