const httpStatus = require('http-status');
const { PhaseProgression } = require('../models');
const ApiError = require('../utils/ApiError');


const createPhaseProgression = async(body) => {
    console.log(body);
    const create = await PhaseProgression.create(body);

    return create;
}

const deletePhaseProgressionById = async (params) => {
  const phaseProgression = await PhaseProgression.findByIdAndRemove(params);
  if (!phaseProgression) {
    throw new ApiError(httpStatus.NOT_FOUND, 'phaseProgression Id not found');
  }
  return phaseProgression;
};

const updatePhaseProgressionById = async (params, body) => {
  const phaseProgression = await PhaseProgression.findByIdAndUpdate(params, body);
  if (!phaseProgression) {
    throw new ApiError(httpStatus.NOT_FOUND, 'phaseProgression Id not found');
  }
  return phaseProgression;
};

const getAllPhaseProgression = async (filter) => {
    const phaseProgression = await PhaseProgression.find(filter);
    return phaseProgression;
  };

module.exports = {
    createPhaseProgression,
    deletePhaseProgressionById,
    updatePhaseProgressionById,
    getAllPhaseProgression,
}