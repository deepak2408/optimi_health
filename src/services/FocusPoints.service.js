const httpStatus = require('http-status');
const { FocusPoints  } = require('../models');
const ApiError = require('../utils/ApiError');

const createFocus = async(body) => {
    const create = await FocusPoints.create(body);
    return create;
}

const getFocus = async(filter) => {
    const getAll = await FocusPoints.find(filter);
    return getAll;
}

const deleteFocusPointsById = async (params) => {
  const deleteFocus = await FocusPoints.findByIdAndRemove(params);
  if (!deleteFocus) {
    throw new ApiError(httpStatus.NOT_FOUND, 'focusPoints id not found');
  }
  return deleteFocus;
};

const updateFocusPointsById = async (params, body) => {
  const updateFocus = await FocusPoints.findByIdAndUpdate(params, body);
  if (!updateFocus) {
    throw new ApiError(httpStatus.NOT_FOUND, 'focusPoints id not found');
  }
  return updateFocus;
};

module.exports = {
    createFocus,
    getFocus,
    deleteFocusPointsById,
    updateFocusPointsById
}