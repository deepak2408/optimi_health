const httpStatus = require('http-status');
const { Partnerships } = require('../models');
const ApiError = require('../utils/ApiError');


const createPartnerships = async(body) => {
    console.log(body);
    const create = await Partnerships.create(body);
    return create;
}

const deletePartnershipsById = async (params) => {
  const partnerships = await Partnerships.findByIdAndRemove(params);
  if (!partnerships) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Partnerships Id not found');
  }
  return partnerships;
};

const updatePartnershipsById = async (params, body) => {
  const partnerships = await Partnerships.findByIdAndUpdate(params, body);
  if (!partnerships) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Partnerships Id not found');
  }
  return partnerships;
};

const getAllPartnerships = async (filter) => {
    const partnerships = await Partnerships.find(filter);
    return partnerships;
  };

module.exports = {
    createPartnerships,
    deletePartnershipsById,
    updatePartnershipsById,
    getAllPartnerships,
}