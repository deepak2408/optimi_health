const httpStatus = require('http-status');
const { ExclusionCriteria  } = require('../models');
const ApiError = require('../utils/ApiError');

const createExCriteria = async(body) => {
    const create = await ExclusionCriteria.create(body);
    return create;
}

const getExCriteria = async(filter) => {
    const getAll = await ExclusionCriteria.find(filter);
    return getAll;
}

const deleteExCriteriaById = async (params) => {
  const deleteExclusion = await ExclusionCriteria.findByIdAndRemove(params);
  if (!deleteExclusion) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ExclusionCriteria id not found');
  }
  return deleteExclusion;
};

const updateExCriteriaById = async (params, body) => {
  const updateExclusion = await ExclusionCriteria.findByIdAndUpdate(params, body);
  if (!updateExclusion) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ExclusionCriteria id not found');
  }
  return updateExclusion;
};

module.exports = {
    createExCriteria,
    getExCriteria,
    deleteExCriteriaById,
    updateExCriteriaById,
}