const httpStatus = require('http-status');
const { ProgramLevel  } = require('../models');
const ApiError = require('../utils/ApiError');

const createProgram = async(body) => {
    const create = await ProgramLevel.create(body);
    return create;
}

const getProgram = async(filter) => {
    const getAll = await ProgramLevel.find(filter);
    return getAll;
}

const deleteProgramById = async (params) => {
  const deleteProgram = await ProgramLevel.findByIdAndRemove(params);
  if (!deleteProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'programLevel id not found');
  }
  return deleteProgram;
};

const updateProgramById = async (params, body) => {
  const updateProgram = await ProgramLevel.findByIdAndUpdate(params, body);
  if (!updateProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'programLevel id not found');
  }
  return updateProgram;
};

module.exports = {
    createProgram,
    getProgram,
    deleteProgramById,
    updateProgramById
}