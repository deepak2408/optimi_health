const httpStatus = require('http-status');
const { ProgramGoal  } = require('../models');
const ApiError = require('../utils/ApiError');

const createProgram = async(body) => {
    const create = await ProgramGoal.create(body);
    return create;
}

const getProgram = async(filter) => {
    const getAll = await ProgramGoal.find(filter);
    return getAll;
}

const deleteProgramById = async (params) => {
  const deleteProgram = await ProgramGoal.findByIdAndRemove(params);
  if (!deleteProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'programGoal id not found');
  }
  return deleteProgram;
};

const updateProgramById = async (params, body) => {
  const updateProgram = await ProgramGoal.findByIdAndUpdate(params, body);
  if (!updateProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'programGoal id not found');
  }
  return updateProgram;
};

module.exports = {
    createProgram,
    getProgram,
    deleteProgramById,
    updateProgramById
}