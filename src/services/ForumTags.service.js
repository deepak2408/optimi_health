const httpStatus = require('http-status');
const { ForumTags } = require('../models');
const ApiError = require('../utils/ApiError');


const createForumTags = async(body) => {
    console.log(body);
    const create = await ForumTags.create(body);
    return create;
}

const deleteForumTagsById = async (params) => {
  const forumTags = await ForumTags.findByIdAndRemove(params);
  if (!forumTags) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ForumTags Id not found');
  }
  return forumTags;
};

const updateForumTagsById = async (params, body) => {
  const forumTags = await ForumTags.findByIdAndUpdate(params, body);
  if (!forumTags) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ForumTags Id not found');
  }
  return forumTags;
};

const getAllForumTags = async (filter) => {
    const forumTags = await ForumTags.find(filter);
    return forumTags;
  };

module.exports = {
    createForumTags,
    deleteForumTagsById,
    updateForumTagsById,
    getAllForumTags,
}