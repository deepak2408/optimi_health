const httpStatus = require('http-status');
const {  Program } = require('../models');
const ApiError = require('../utils/ApiError');

const createProgram = async( body) => {
   const program = await Program.create(body);
   return program;
    
}

const deleteProgramById = async (params) => {
  const deleteProgram = await Program.findByIdAndRemove(params);
  if (!deleteProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Program Id not found');
  }
  return deleteProgram;
};

const queryProgram = async (filter, options) => {
  const program = await Program.paginate(filter, options);
  return program;
};

const getProgramById = async (params) => {
  const getProgram = await Program.findById(params);
  if (!getProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Program id not found');
  }
  return getProgram;
};

const updateProgramById = async (params, body) => {
  const updateProgram = await Program.findByIdAndUpdate(params, body);
  if (!updateProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Program id not found');
  }
  return updateProgram;
};

module.exports = {
    createProgram,
    deleteProgramById,
    queryProgram,
    updateProgramById,
    getProgramById
}