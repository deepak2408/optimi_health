const httpStatus = require('http-status');
const { Tags } = require('../models');
const ApiError = require('../utils/ApiError');


const createTag = async(body) => {
    const tags = await Tags.create(body);
    return tags;
}

const deleteTagById = async (params) => {
  const tags = await Tags.findByIdAndRemove(params);
  if (!tags) {
    throw new ApiError(httpStatus.NOT_FOUND, 'tag Id not found');
  }
  return tags;
};

const updateTagById = async (params, body) => {
  const tags = await Tags.findByIdAndUpdate(params, body);
  if (!tags) {
    throw new ApiError(httpStatus.NOT_FOUND, 'tag Id not found');
  }
  return tags;
};

const getAllTags = async (filter) => {
    const tags = await Tags.find(filter);
   // console.log(tags);
    return tags;
  };

module.exports = {
    createTag,
    deleteTagById,
    updateTagById,
    getAllTags,
}