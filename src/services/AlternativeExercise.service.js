const httpStatus = require('http-status');
const { AlternativeExercise  } = require('../models');
const ApiError = require('../utils/ApiError');

const createAltExercise = async(body) => {
    const create = await AlternativeExercise.create(body);
    return create;
}

const getAltExercise = async(filter) => {
    const getAll = await AlternativeExercise.find(filter);
    return getAll;
}

const deleteAltExerciseById = async (params) => {
  const deleteExercise = await AlternativeExercise.findByIdAndRemove(params);
  if (!deleteExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'AlternativeExercise id not found');
  }
  return deleteExercise;
};

const updateAltExerciseById = async (params, body) => {
  const updateExercise = await AlternativeExercise.findByIdAndUpdate(params, body);
  if (!updateExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'AlternativeExercise id not found');
  }
  return updateExercise;
};

module.exports = {
    createAltExercise,
    getAltExercise,
    deleteAltExerciseById,
    updateAltExerciseById,
}