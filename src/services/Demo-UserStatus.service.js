const httpStatus = require('http-status');
const { DemoUserStatus } = require('../models');
const ApiError = require('../utils/ApiError');

const createDemo = async (body) => {
  //console.log(body);
  const demoUser = await DemoUserStatus.findOne({ uniqueId: body.uniqueId });
  console.log(demoUser);
  if (!demoUser) {
    const demo = await DemoUserStatus.create(body);
    return demo;
  } else {
    Object.assign(demoUser, body);
    await demoUser.save();
    return demoUser;
  }
};

module.exports = { createDemo };
