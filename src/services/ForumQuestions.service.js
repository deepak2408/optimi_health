const httpStatus = require('http-status');
const { ForumQuestion } = require('../models');
const ApiError = require('../utils/ApiError');

const createForumQuestion = async (body) => {
  const forumQuestion = await ForumQuestion.create(body);
  return forumQuestion;
};

const deleteForumQuestionById = async (params) => {
  const deleteForum = await ForumQuestion.findByIdAndRemove(params);
  if (!deleteForum) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ForumQuestion Id not found');
  }
  return deleteForum;
};

const queryForumQuestion = async (filter, options) => {
  const forumQuestion = await ForumQuestion.paginate(filter, options);
  return forumQuestion;
};

const getForumQuesitonById = async (params) => {
  const getForumQuestion = await ForumQuestion.findById(params);
  if (!getForumQuestion) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ForumQuestion id not found');
  }
  return getForumQuestion;
};

const updateForumQuestionById = async (params, body) => {
  const updateForumQuestion = await ForumQuestion.findByIdAndUpdate(params, body);
  if (!updateForumQuestion) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ForumQuestion id not found');
  }
  return updateForumQuestion;
};

module.exports = {
  createForumQuestion,
  deleteForumQuestionById,
  queryForumQuestion,
  updateForumQuestionById,
  getForumQuesitonById,
};
