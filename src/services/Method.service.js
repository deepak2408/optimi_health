const httpStatus = require('http-status');
const { Method } = require('../models');
const ApiError = require('../utils/ApiError');


const createMethod = async(body) => {
    console.log(body);
    const create = await Method.create(body);
    return create;
}

const deleteMethodById = async (params) => {
  const method = await Method.findByIdAndRemove(params);
  if (!method) {
    throw new ApiError(httpStatus.NOT_FOUND, 'method Id not found');
  }
  return method;
};

const updateMethodById = async (params, body) => {
  const method = await Method.findByIdAndUpdate(params, body);
  if (!method) {
    throw new ApiError(httpStatus.NOT_FOUND, 'method Id not found');
  }
  return method;
};

const getAllMethod = async (filter) => {
    const method = await Method.find(filter);
    return method;
  };

module.exports = {
    createMethod,
    deleteMethodById,
    updateMethodById,
    getAllMethod,
}