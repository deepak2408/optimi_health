const httpStatus = require('http-status');
const { RegressionExercise  } = require('../models');
const ApiError = require('../utils/ApiError');

const createRegExercise = async(body) => {
    const create = await RegressionExercise.create(body);
    return create;
}

const getRegExercise = async(filter) => {
    const getAll = await RegressionExercise.find(filter);
    return getAll;
}

const deleteRegExerciseById = async (params) => {
  const deleteExercise = await RegressionExercise.findByIdAndRemove(params);
  if (!deleteExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'RegressionExercise id not found');
  }
  return deleteExercise;
};

const updateRegExerciseById = async (params, body) => {
  const updateExercise = await RegressionExercise.findByIdAndUpdate(params, body);
  if (!updateExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'RegressionExercise id not found');
  }
  return updateExercise;
};

module.exports = {
    createRegExercise,
    getRegExercise,
    deleteRegExerciseById,
    updateRegExerciseById,
}