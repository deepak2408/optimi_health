const httpStatus = require('http-status');
const { Admin } = require('../models');
const ApiError = require('../utils/ApiError');

const loginAdmin = async (body) => {
  const findAdmin = await Admin.findOne({ username: body.username });
  if (!findAdmin) {
    throw new ApiError(httpStatus.NOT_FOUND, 'not found');
  }
  if (!(findAdmin.password === body.password)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'not matched');
  }
  return findAdmin;
};

module.exports = {
  loginAdmin,
};
