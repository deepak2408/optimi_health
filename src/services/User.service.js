const httpStatus = require('http-status');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');

const createUser = async (body) => {
  console.log(body);
  const create = await User.create(body);
  return create;
};

const getUser = async (filter, options) => {
  const getAll = await User.paginate(filter, options);
  return getAll;
};

const deleteUserById = async (params) => {
  const deleteUser = await User.findByIdAndRemove(params);
  if (!deleteUser) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'user id not found');
  }
  return deleteUser;
};

const loginUser = async (body) => {
  const findUser = await User.findOne({ email: body.email });
  if (!findUser) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional email not found');
  }
  if (!(findUser.password === body.password)) {
    throw new ApiError(httpStatus.BAD_REQUEST, ' password not matched');
  }
  return findUser;
};

module.exports = {
  createUser,
  getUser,
  deleteUserById,
  loginUser,
};
