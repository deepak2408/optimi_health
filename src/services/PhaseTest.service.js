const httpStatus = require('http-status');
const { PhaseTest } = require('../models');
const ApiError = require('../utils/ApiError');


const createPhaseTest = async (body) => {
  console.log(body);
  const create = await PhaseTest.create(body);
  return create;
};

const deletePhaseTestById = async (params) => {
  const phaseTest = await PhaseTest.findByIdAndRemove(params);
  if (!phaseTest) {
    throw new ApiError(httpStatus.NOT_FOUND, 'phaseTest Id not found');
  }
  return phaseTest;
};

const updatePhaseTestById = async (params, body) => {
  const phaseTest = await PhaseTest.findByIdAndUpdate(params, body);
  if (!phaseTest) {
    throw new ApiError(httpStatus.NOT_FOUND, 'phaseTest Id not found');
  }
  return phaseTest;
};

const getAllPhaseTest = async (filter) => {
    const phaseTest = await PhaseTest.find(filter);
    return phaseTest;
  };

module.exports = {
    createPhaseTest,
    deletePhaseTestById,
    updatePhaseTestById,
    getAllPhaseTest,
}