const httpStatus = require('http-status');
const { ProgramPhases  } = require('../models');
const ApiError = require('../utils/ApiError');

const createProgram = async(body) => {
    const create = await ProgramPhases.create(body);
    return create;
}

const getProgram = async(filter) => {
    const getAll = await ProgramPhases.find(filter);
    return getAll;
}

const deleteProgramById = async(params) => {
    const deleteProgram = await ProgramPhases.findByIdAndRemove(params);
    if(! deleteProgram) {
        throw new ApiError(httpStatus.BAD_REQUEST, 'programPhase id not found');
    }
    return deleteProgram;
}

const updateProgramById = async(params, body) => {
    const updateProgram = await ProgramPhases.findByIdAndUpdate(params, body);
    if(! updateProgram) {
        throw new ApiError(httpStatus.BAD_REQUEST, 'programPhase id not found');
    }
    return updateProgram;
}

module.exports = {
    createProgram,
    getProgram,
    deleteProgramById,
    updateProgramById
}