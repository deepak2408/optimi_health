const httpStatus = require('http-status');
const {  Exercises } = require('../models');
const ApiError = require('../utils/ApiError');

const createExercise = async(body) => {
    console.log(body);
   const exercise = await Exercises.create(body);
   return exercise;
    
}

const deleteExerciseById = async (params) => {
  const deleteexercise = await Exercises.findByIdAndRemove(params);
  if (!deleteexercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Exercise Id not found');
  }
  return deleteexercise;
};

const queryExercises = async (filter, options) => {
  const exercise = await Exercises.paginate(filter, options);
  return exercise;
};

const getExerciseById = async (params) => {
  const getExercise = await Exercises.findById(params);
  if (!getExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Exercise id not found');
  }
  return getExercise;
};

const updateExerciseById = async (params, body) => {
  const updateexercise = await Exercises.findByIdAndUpdate(params, body);
  if (!updateexercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'deleteexercise id not found');
  }
  return updateexercise;
};

module.exports = {
    createExercise,
    deleteExerciseById,
    queryExercises,
    updateExerciseById,
    getExerciseById
}