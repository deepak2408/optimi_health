const httpStatus = require('http-status');
const { Plan } = require('../models');
const ApiError = require('../utils/ApiError');

const createPlan = async (body) => {
  const plan = await Plan.create(body);
  return plan;
};

const deletePlanById = async (params) => {
  const deletePlan = await Plan.findByIdAndRemove(params);
  if (!deletePlan) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Plan Id not found');
  }
  return deletePlan;
};

const queryPlan = async (filter, options) => {
  const plan = await Plan.paginate(filter, options);
  return plan;
};

const getPlanById = async (params) => {
  const getPlan = await Plan.findById(params);
  if (!getPlan) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Plan id not found');
  }
  return getPlan;
};

const updatePlanById = async (params, body) => {
  const updatePlan = await Plan.findByIdAndUpdate(params, body);
  if (!updatePlan) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Plan id not found');
  }
  return updatePlan;
};

module.exports = {
  createPlan,
  deletePlanById,
  queryPlan,
  updatePlanById,
  getPlanById,
};
