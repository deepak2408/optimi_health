const httpStatus = require('http-status');
const { error } = require('winston');
const { Professional } = require('../models');
const ApiError = require('../utils/ApiError');

const createProfessional = async (body) => {
  const create = await Professional.create(body);
  return create;
};

const getProfessional = async (filter, options) => {
  const getAll = await Professional.find(filter, options);
  return getAll;
};

const getProfessionalById = async (params) => {
  const getProfessional = await Professional.findById(params);
  if (!getProfessional) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional id not found');
  }
  return getProfessional;
};

const deleteProfessionalById = async (params) => {
  const professional = await Professional.findByIdAndRemove(params);
  if (!professional) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional id not found');
  }
  return professional;
};

const updateProfessionalById = async (params, body) => {
  const professional = await Professional.findByIdAndUpdate(params, body);
  if (!professional) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional id not found');
  }
  return professional;
};

const checkProfessional = async (params, body) => {
  const findProfessional = await Professional.findByIdAndUpdate(params, body);
  if (!findProfessional) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional id not found');
  }
  return findProfessional;
};

const loginProfessional = async (body) => {
  const findProfessional = await Professional.findOne({ email: body.email });
  if (!findProfessional) {
    throw new ApiError(httpStatus.NOT_FOUND, 'professional email not found');
  }
  if (findProfessional.status === false) {
    throw new Error('your status is InActive! please contact Admin ');
  }
  if (!(findProfessional.password === body.password)) {
    throw new ApiError(httpStatus.BAD_REQUEST, ' password not matched');
  }
  return findProfessional;
};

module.exports = {
  createProfessional,
  deleteProfessionalById,
  updateProfessionalById,
  getProfessional,
  getProfessionalById,
  checkProfessional,
  loginProfessional,
};
