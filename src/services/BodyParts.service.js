const httpStatus = require('http-status');
const { BodyParts  } = require('../models');
const ApiError = require('../utils/ApiError');

const createPartsOfBody = async(body) => {
    const createPartsOfBody = await BodyParts.create(body);
    return createPartsOfBody;
}

const getPartsOfBody = async(filter) => {
    const getPartsOfBody = await BodyParts.find(filter);
    return getPartsOfBody;
}

const deletePartsOfBodyById = async (params) => {
  const deletePartsOfBody = await BodyParts.findByIdAndRemove(params);
  if (!deletePartsOfBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'bodyParts id not found');
  }
  return deletePartsOfBody;
};

const updatePartsOfBodyById = async (params, body) => {
  const updatePartsOfBody = await BodyParts.findByIdAndUpdate(params, body);
  if (!updatePartsOfBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'bodyParts id not found');
  }
  return updatePartsOfBody;
};

module.exports = {
    createPartsOfBody,
    getPartsOfBody,
    deletePartsOfBodyById,
    updatePartsOfBodyById,
}