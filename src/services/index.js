module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.tagService = require('./tag.service');
module.exports.postService = require('./post.service');
module.exports.programGoalService = require('./P-Goal.service');
module.exports.programLevelService = require('./P-Level.service');
module.exports.programPhasesService = require('./P-Phases.service');
module.exports.programService = require('./P-Program.service');
module.exports.bodyPartsService = require('./BodyParts.service');
module.exports.focusPointsService = require('./FocusPoints.service');
module.exports.equipmentsService = require('./Equipments.service');
module.exports.exerciseTypeService = require('./ExerciseType.service');
module.exports.exclusionCriteriaService = require('./ExclusionCriteria.service');
module.exports.regressionExerciseService = require('./RegressionExercise.service');
module.exports.progressionExerciseService = require('./ProgressionExercise.servise');
module.exports.alternativeExerciseService = require('./AlternativeExercise.service')
module.exports.exerciseService = require('./Exercises.service');
module.exports.methodService = require('./Method.service');
module.exports.sessionService = require('./Session.service');
module.exports.partnershipsService = require('./Partnerships.service');
module.exports.forumTagsService = require('./ForumTags.service');
module.exports.phasesService = require('./Phases.service');
module.exports.phaseTestService = require('./PhaseTest.service');
module.exports.phaseProgressionService = require('./PhaseProgression.service')
module.exports.professionalService = require('./Professional.service');
module.exports.adminService = require('./optimi-admin.service');
module.exports.forumQuestionService = require('./ForumQuestions.service');
module.exports.planService = require('./Plan.service');
module.exports.selfManageProgramService = require('./SelfManageProgram.service');
module.exports.userService = require('./User.service');
module.exports.demoUserStatusService = require('./Demo-UserStatus.service');
