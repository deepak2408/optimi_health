const httpStatus = require('http-status');
const { Post, Tags } = require('../models');
const ApiError = require('../utils/ApiError');

const createPost = async (body) => {
  const searchTag = await Tags.findById(body.tag);
  if (!searchTag) {
    throw new ApiError(httpStatus.NOT_FOUND, 'tag _id not found');
  }
  const post = await Post.create(body);
  return post;
};

const deletePostById = async (params) => {
  const post = await Post.findByIdAndRemove(params);
  if (!post) {
    throw new ApiError(httpStatus.NOT_FOUND, 'post Id not found');
  }
  return post;
};

const queryPosts = async (filter, options) => {
  const posts = await Post.paginate(filter, options);
  return posts;
};

const getPostById = async (params) => {
  const getPost = await Post.findById(params);
  if (!getPost) {
    throw new ApiError(httpStatus.NOT_FOUND, 'post id not found');
  }
  return getPost;
};

const updatePostById = async (params, body) => {
  const searchTag = await Tags.findOne({ _id: body.tag });
  if (!searchTag) {
    throw new ApiError(httpStatus.NOT_FOUND, 'tag id not found');
  }
  const posts = await Post.findByIdAndUpdate(params, body);
  if (!posts) {
    throw new ApiError(httpStatus.NOT_FOUND, 'post id not found');
  }
  return posts;
};

module.exports = {
    createPost,
    deletePostById,
    queryPosts,
    updatePostById,
    getPostById
}