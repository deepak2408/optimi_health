const httpStatus = require('http-status');
const {  Phases } = require('../models');
const ApiError = require('../utils/ApiError');

const createPhases = async( body) => {
   const phases = await Phases.create(body);
   return phases;
    
}

const deletePhasesById = async (params) => {
  const deletePhases = await Phases.findByIdAndRemove(params);
  if (!deletePhases) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Phases Id not found');
  }
  return deletePhases;
};

const queryPhases = async (filter, options) => {
  const phases = await Phases.paginate(filter, options);
  return phases;
};

const getPhaseById = async (params) => {
  const getPhase = await Phases.findById(params);
  if (!getPhase) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Phases id not found');
  }
  return getPhase;
};

const updatePhasesById = async (params, body) => {
  const updatePhases = await Phases.findByIdAndUpdate(params, body);
  if (!updatePhases) {
    throw new ApiError(httpStatus.NOT_FOUND, 'deletePhases id not found');
  }
  return updatePhases;
};

module.exports = {
    createPhases,
    deletePhasesById,
    queryPhases,
    updatePhasesById,
    getPhaseById
}