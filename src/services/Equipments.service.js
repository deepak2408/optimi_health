const httpStatus = require('http-status');
const { Equipments  } = require('../models');
const ApiError = require('../utils/ApiError');

const createEquip = async(body) => {
    const create = await Equipments.create(body);
    return create;
}

const getEquip = async(filter) => {
    const getAll = await Equipments.find(filter);
    return getAll;
}

const deleteEquipById = async (params) => {
  const deleteEquip = await Equipments.findByIdAndRemove(params);
  if (!deleteEquip) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Equipments id not found');
  }
  return deleteEquip;
};

const updateEquipById = async (params, body) => {
  const updateEquip = await Equipments.findByIdAndUpdate(params, body);
  if (!updateEquip) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Equipments id not found');
  }
  return updateEquip;
};

module.exports = {
    createEquip,
    getEquip,
    deleteEquipById,
    updateEquipById,
}