const httpStatus = require('http-status');
const { SelfManageProgram } = require('../models');
const ApiError = require('../utils/ApiError');

const createSelfManageProgram = async (body) => {
  const { stageCount, stages, name } = body;
  if (!stageCount || !stages || stageCount !== stages.length) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'stageCount, stages are required and there length should match.');
  }
  const selfManageProgram = await SelfManageProgram.create(body);
  return selfManageProgram;
};

const deleteSelfManageProgramById = async (params) => {
  const deleteSelfManageProgram = await SelfManageProgram.findByIdAndRemove(params);
  if (!deleteSelfManageProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'SelfManageProgram id not found');
  }
  return deleteSelfManageProgram;
};

const querySelfManageProgram = async (filter, options) => {
  console.log(filter, options);
  const selfManageProgram = await SelfManageProgram.paginate(filter, options);
  return selfManageProgram;
};

const getSelfManageProgramById = async (params) => {
  const getSelfManageProgram = await SelfManageProgram.findById(params);
  if (!getSelfManageProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'SelfManageProgram id not found');
  }
  return getSelfManageProgram;
};

const updateSelfManageProgramById = async (params, body) => {
  const { stageCount, stages, name } = body;
  if (!stageCount || !stages || stageCount !== stages.length) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'stageCount, stages are required and there length should match.');
  }
  const updateSelfManageProgram = await SelfManageProgram.findByIdAndUpdate(params, body);
  if (!updateSelfManageProgram) {
    throw new ApiError(httpStatus.NOT_FOUND, 'deleteSelfManageProgram id not found');
  }
  return updateSelfManageProgram;
};

module.exports = {
  createSelfManageProgram,
  deleteSelfManageProgramById,
  querySelfManageProgram,
  updateSelfManageProgramById,
  getSelfManageProgramById,
};
