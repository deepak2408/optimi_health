const httpStatus = require('http-status');
const {  Session } = require('../models');
const ApiError = require('../utils/ApiError');

const createSession = async( body) => {
   const session = await Session.create(body);
   return session;
    
}

const deleteSessionById = async (params) => {
  const session = await Session.findByIdAndRemove(params);
  if (!session) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Session Id not found');
  }
  return session;
};

const querySession = async (filter, options) => {
  const session = await Session.paginate(filter, options);
  return session;
};

const getSessionById = async (params) => {
  const session = await Session.findById(params);
  if (!session) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Session id not found');
  }
  return session;
};

const updateSessionById = async (params, body) => {
  const session = await Session.findByIdAndUpdate(params, body);
  if (!session) {
    throw new ApiError(httpStatus.NOT_FOUND, 'session id not found');
  }
  return session;
};

module.exports = {
    createSession,
    deleteSessionById,
    querySession,
    updateSessionById,
    getSessionById
}