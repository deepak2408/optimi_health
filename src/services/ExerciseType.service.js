const httpStatus = require('http-status');
const { ExercisType  } = require('../models');
const ApiError = require('../utils/ApiError');

const createExerciseTyp = async(body) => {
    const create = await ExercisType.create(body);
    return create;
}

const getExerciseTyp = async(filter) => {
    const getAll = await ExercisType.find(filter);
    return getAll;
}

const deleteExerciseTypById = async (params) => {
  const deleteExercise = await ExercisType.findByIdAndRemove(params);
  if (!deleteExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ExercisType id not found');
  }
  return deleteExercise;
};

const updateExerciseTypById = async (params, body) => {
  const updateExercise = await ExercisType.findByIdAndUpdate(params, body);
  if (!updateExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ExercisType id not found');
  }
  return updateExercise;
};

module.exports = {
    createExerciseTyp,
    getExerciseTyp,
    deleteExerciseTypById,
    updateExerciseTypById,
}