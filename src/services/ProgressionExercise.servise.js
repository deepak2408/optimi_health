const httpStatus = require('http-status');
const { ProgressionExercise  } = require('../models');
const ApiError = require('../utils/ApiError');

const createProgExercise = async(body) => {
    const create = await ProgressionExercise.create(body);
    return create;
}

const getProgExercise = async(filter) => {
    const getAll = await ProgressionExercise.find(filter);
    return getAll;
}

const deleteProgExerciseById = async (params) => {
  const deleteExercise = await ProgressionExercise.findByIdAndRemove(params);
  if (!deleteExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ProgressionExercise id not found');
  }
  return deleteExercise;
};

const updateProgExerciseById = async (params, body) => {
  const updateExercise = await ProgressionExercise.findByIdAndUpdate(params, body);
  if (!updateExercise) {
    throw new ApiError(httpStatus.NOT_FOUND, 'ProgressionExercise id not found');
  }
  return updateExercise;
};

module.exports = {
    createProgExercise,
    getProgExercise,
    deleteProgExerciseById,
    updateProgExerciseById,
}